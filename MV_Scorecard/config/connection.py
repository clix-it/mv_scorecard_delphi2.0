import pandas as pd
import psycopg2
import logging
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Database:
    def __init__(self, conn=None):
        self.scorecard = 'd2c_scorecard'
        self.db_scorecard = None
        self.config_key = None
        self.conn = conn
        self.operations = None
        self.config_value = None
        self.mb_account_type_id_dict = {}
        self.account_type_dict = {}
        self.negative_area_dict = {}
        self.emi_for_acct_type_list = pd.read_csv('emi_for_Acctypes.csv', index_col=0).to_dict(orient='index')
        self.cursor = self.database_connection(conn)
        self.list_data = self.get_scorecard_config()

    def database_connection(self, conn):
        try:
            # create a cursor
            self.cursor = conn.cursor()

            return self.cursor
        except Exception as e:
            logger.error(e)
            return logger.error(e)

    def get_scorecard_config(self):
        global data_list
        try:
            data_list = []
            # query = "select * from scorecard_config where scorecard = '{}';".format(self.scorecard)
            # self.cursor.execute(query)
            # rows = self.cursor.fetchall()
            # for row in rows:
            #     self.db_scorecard = row[1]
            #     self.config_key = row[2]
            #     self.operations = row[3]
            #     self.config_value = row[4]
            #     data_list.append(row)

            # print("database configuration rows")
            # # logger.info(data_list)
            query_acc_id = "select * from account_type_id;"
            self.cursor.execute(query_acc_id)
            rows = self.cursor.fetchall()
            for row in rows:
                self.mb_account_type_id_dict[row[1]] = row[2]
                self.account_type_dict[(row[4])] = [row[1], row[2]]

            # emi_for_acct_type = "select * from emi_for_acct_type where type_xml = 'mb';"
            # self.cursor.execute(emi_for_acct_type)
            # rows = self.cursor.fetchall()
            # for row in rows:
            #     emi_dict = {}
            #     emi_dict[row[0]] = {"roi": row[1], "tenure": row[2], "max_amount": row[3],
            #                         "min_amount": row[4]}
            #     self.emi_for_acct_type_list.append(emi_dict)

        except Exception as error:
            if self.cursor is not None:
                self.conn.rollback()
                msg = "Failed to run query"
                logger.error(msg, error)

        return data_list
