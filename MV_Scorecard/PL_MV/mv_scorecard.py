import logging
import numpy as np
import dateutil.relativedelta
import pandas as pd
import numpy_financial as npf
from functions.conf_functions import *
from common_functions import *
from fuzzywuzzy import fuzz
from functools import reduce
import pkg_resources
from PL_MV.rejection_reasons import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def pq_consumer_bureau(request, bureau, database, connection, typeXML, applicationstage, bureau_message, is_ntc):
    warnings = {}
    loan_app_id = request["applicationId"]
    qde = request['report']['applicant']['qde']
    banking_mode = deep_get(request, 'report.stage.bankingMode', None)
    additional_details = deep_get(request, 'report.applicant.extraDetails', {})
    try:
        # create a cursor
        cursor = connection.cursor()
    except Exception as e:
        logger.error(e)
        warnings["DataBase"] = "database connection break!"
        return {}, [], warnings

    is_mobile = False
    mobile = "Initializing Number"
    for phone in qde['phones']:
        if phone['phoneType'].upper() in ['MOBILE', 'PHONE', 'PERSONAL']:
            mobile = phone_cleaner(phone['phoneNumber'])
            is_mobile = True
            break
    try:
        bureau_mobile = bureau.application_details.phone_variation
    except:
        bureau_mobile = None

    try:
        mobileTest = qde['mobileTest']
        pol_mobile_match = bool(mobileTest)
    except:
        try:
            pol_mobile_match = True if mobile in bureau_mobile else False
        except:
            pol_mobile_match = False

    try:
        employment_type = str(qde['occupationDetails']['employmentType']).upper().replace(' ', '')
    except:
        try:
            employment_type = "SALARIED" if bool(qde['salaried']) else 'SELF_EMPLOYMENT'
        except:
            employment_type = None
        warnings["employment_type"] = "employment_type tag is mandatory"

    pol_employment_test = True if employment_type not in ["SELF_EMPLOYMENT", "SELF_EMPLOYED", "SELFEMPLOYED", "",
                                                          'NONE', "SELF EMPLOYED",
                                                          None] else False

    if employment_type in ["SELF_EMPLOYMENT", "SELF_EMPLOYED", "SELFEMPLOYED", "SELF EMPLOYED"]:
        try:
            monthly_income = float(
                str(qde['occupationDetails']['monthlySalary']).replace(',', ''))
            monthly_income = monthly_income if monthly_income > 0 else float(
                str(qde['occupationDetails']['annualTurnover']).replace(',', '')) / 12
        except:
            try:
                monthly_income = float(
                    str(qde['occupationDetails']['annualTurnover']).replace(',', '')) / 12
            except:
                monthly_income = 0
                warnings["monthlySalary"] = 'Salary is mandatory'
        try:
            is_annual_income = bool(qde['occupationDetails']['isAnnualIncome'])
        except:
            is_annual_income = False

        monthly_income = monthly_income / 12 if is_annual_income else monthly_income

    elif employment_type in ["SALARIED"]:
        try:
            monthly_income = float(str(qde['occupationDetails']['monthlySalary']).replace(',', ''))
        except:
            monthly_income = 0.0

    else:
        monthly_income = 0.0

    validated_monthly_salary = additional_details.get('validatedMonthlySalary', False)

    if validated_monthly_salary is True:
        monthly_income = float(additional_details['validatedMonthlySalaryAmount'])

    loanDetails = request['report']["loanDetails"]

    try:
        loan_amount = int(loanDetails.get('loanAmount', 0))
    except:
        loan_amount = 0

    if loan_amount < 0:
        loan_amount = 0

    offered_loan_amount = loan_amount

    try:
        tenure = int(str(loanDetails.get("receivedTenure", 0)))
    except:
        tenure = 0

    try:
        requested_pf = float(str(loanDetails.get("pf", '2')).replace('%', ''))
    except:
        requested_pf = 2

    if requested_pf < 2:
        requested_pf = 2

    try:
        requested_roi = float(str(loanDetails.get("receivedROI", '0')).replace('%', ''))
    except:
        requested_roi = 0

    try:
        end_obligation = float(str(loanDetails.get("endObligation", 0)).replace(",", ""))
    except:
        end_obligation = 0


    try:
        loan_flag = str(additional_details['loanFlag']).upper()
    except:
        loan_flag = None

    try:
        scheme_name = str(additional_details['schemeName']).upper()
    except:
        scheme_name = None

    try:
        risk_segment = str(additional_details['riskSegment'])
    except:
        risk_segment = None

    scheme_priority_value = calculate_scheme_priority(scheme_name, loan_flag, employment_type)

    risk_band_value, scheme_priority_band, verified_income_band = define_segment_values(risk_segment,
                                                                                         scheme_priority_value,
                                                                                         monthly_income)

    R2_value = risk_band_value if risk_band_value != 'unknown risk' else 0
    S2_value = scheme_priority_band if scheme_priority_band != 'unknown scheme priority' else 0

    tenure_capped = calculate_tenor_cap(R2_value, S2_value)
    tenure_factor = get_tenure_factor(tenure_capped)
    loan_amount_cap_l1 = calculate_loan_amount_cap(R2_value, S2_value)
    emi_multiplier_cap = calculate_emi_multiplier_cap(R2_value, S2_value)
    #max_emi = calculate_max_emi(monthly_income, end_obligation, emi_multiplier_cap)

    emi_1 = max((0.8 * monthly_income - end_obligation) * emi_multiplier_cap, 0)
    emi_2 = max(0.7 * monthly_income - end_obligation, 0)
    max_emi = min(emi_1, emi_2)

    loan_multiplier_cap = calculate_loan_multiplier_cap(R2_value, verified_income_band)

    loan_amount_on_roi_tenure_emi_l2 = abs(max(np.pv(requested_roi / 1200, tenure_capped, -max_emi), 0))

    max_loan_amount_l3 = max(loan_multiplier_cap * monthly_income * tenure_factor, 0)

    final_loan_amount = min(loan_amount_cap_l1, loan_amount_on_roi_tenure_emi_l2, max_loan_amount_l3)

    rounded_minimum_loan_amount = max(5000 * round(final_loan_amount / 5000), 0) #checking for negative
    calculated_loan_amount = rounded_minimum_loan_amount
    final_loan_amount = offered_loan_amount

    if rounded_minimum_loan_amount >= offered_loan_amount:
        pol_higher_eligibility_check = True
    else:
        pol_higher_eligibility_check = False #Reject the case

    if (employment_type in ["SALARIED", "salaried"] and 3 <= tenure <= 60) or (employment_type in ["SELF_EMPLOYMENT", "SELF_EMPLOYED", "SELFEMPLOYED", "SELF EMPLOYED"] and 3 <= tenure <= 36):
        pol_tenure_check = True
    else:
        pol_tenure_check = False

    if 16 <= requested_roi <= 36:
        pol_roi_check = True
    else:
        pol_roi_check = False

    application_id = request['applicationId'].strip()
    if application_id not in [None, ""]:
        channel = application_id[:2]
    else:
        channel = "MV"

    '''
    need clarification from the product/risk team regarding the rules: pol_loan_amt_check, 
    as we are calculating the loan amount (line assignment logic).
    '''
    if scheme_name == 'GROWTH':
        pol_loan_amt_check = True if final_loan_amount in range(5000, 25001) else False
    elif final_loan_amount in range(15000, 500001):
        pol_loan_amt_check = True
    else:
        pol_loan_amt_check = False


    ca_city = 'NONE'
    ca_pincode = None
    locality = None
    qde_address = None

    for address in qde['addresses']:
        address_type = address['type'].upper()
        if address_type in ['RESIDENCE', 'CURRENT', 'COMMUNICATION']:
            try:
                qde_address = ", ".join(
                    [address['address1'], address["address2"], address["landMark"],
                     address["locality"],
                     address["city"], address["state"], address["country"], address["pincode"]])
            except:
                qde_address = None
            ca_city = address.get('city', 'NONE')
            ca_pincode = int(address['pincode'])
            try:
                locality = str(address['locality']).upper()
            except:
                locality = None
            break
        elif address_type in ['PERMANENT']:
            try:
                qde_address = ", ".join(
                    [address['address1'], address["address2"], address["landMark"],
                     address["locality"], address["city"], address["state"],
                     address["country"], address["pincode"]])
            except:
                qde_address = None

            ca_city = address.get('city', 'NONE')
            ca_pincode = int(address['pincode'])
            try:
                locality = str(address['locality']).upper()
            except:
                locality = None

    try:
        dob = str(qde['dob'])
    except:
        dob = None

    age = age_calculator(dob)
    if age is not None:
        pol_age_match = True if age in range(21, 59) else False
    else:
        pol_age_match = False
        warnings["DOB"] = "Pass a valid DOB"

    try:
        query = "select pincode from eligible_city_pincode_aug06 where pincode = '{}';".format(
            ca_pincode)
        cursor.execute(query)
        rows = cursor.fetchall()
    except:
        rows = [[]]
    pol_city_pincode_match = True if any(str(ca_pincode) in i for i in rows) else False

    address_match_bureau = pol_city_pincode_match

    if loan_amount > 100000:
        if locality not in ["", None, "NONE", "NULL"]:
            query = "select negative_area from negative_area_list where LOWER(negative_area) = LOWER('{}');".format(
                locality)
            cursor.execute(query)
            rows = cursor.fetchall()
            pol_negative_area_match = True if any(
                locality.lower() in map(str.lower, i) for i in rows) else False
        else:
            query = "select pincode from negative_area_list where pincode = '{}';".format(
                ca_pincode)
            cursor.execute(query)
            rows = cursor.fetchall()
            pol_negative_area_match = True if any(str(ca_pincode) in i for i in rows) else False
    else:
        pol_negative_area_match = False

    pincode_map_bureau = pol_negative_area_match
    ev_perfios_decision = 'NONE'
    applicant = request['report']['applicant']
    # sal_cr_mth1, sal_cr_mth2, sal_cr_avg_l4mth, verified_income, is_perfios_data, bank_statement_status \
    #     = perfios_bank_parser(applicant)
    is_company_name = False

    monthly_income_app = monthly_income
    try:
        address_list = bureau.application_details.address_variation
    except:
        address_list = []
    emp_comp_bureau_match = None

    try:
        if monthly_income >= 15000:
            pol_income_match = True
        else:
            pol_income_match = False
    except:
        pol_income_match = False

    # ====================+ Consumer Bureau +================= #

    try:
        tradelines = bureau.accounts.tradeline.rows
        inquiry_data = bureau.accounts.li
    except:
        tradelines = []
        inquiry_data = []

    if typeXML in ['EXPERIAN', 'EXP'] and bureau is not None:
        tradelines, inquiry_data = acct_inquiry_type_cleaner(database.account_type_dict, tradelines, inquiry_data)

    if typeXML in ['CIBIL', 'CIBIL_JSON']:
        tradelines, inquiry_data = acct_inquiry_type_cleaner_cibil(tradelines, inquiry_data)

    try:
        bureau_name = bureau.name
    except:
        bureau_name = None
        warnings["BureauName"] = "No bureau name in xml"

    try:
        bureau_score = int(bureau.score)
    except:
        bureau_score = 0

    if "consumer record not found" in bureau_message:
        sourcing_dates = None
        current_date = None
    else:
        try:
            sourcing_dates = bureau.sourcing_date
            current_date = (datetime.strptime(sourcing_dates,
                                              '%d-%m-%Y')).date() - dateutil.relativedelta.relativedelta(
                months=1)
        except:
            sourcing_dates = None
            current_date = None
            warnings["bureauDate"] = "Invalid Bureau OR Source Date"

    if bureau_score in [None, math.nan, np.nan, np.NaN]:
        is_ntc = True
    elif int(bureau_score) in range(-1, 11):
        is_ntc = True
    else:
        is_ntc = is_ntc

    if bureau_score is not None and bureau_name is not None:
        if bureau_name.upper() == 'EXPERIAN':
            bureau_status = bureau_score >= 650
            multiple_bureau_check = bureau_score >= 800
        elif bureau_name.upper() == 'CIBIL':
            bureau_status = bureau_score >= 650
            multiple_bureau_check = bureau_score >= 800
        else:
            bureau_status = bureau_score >= 650
            multiple_bureau_check = bureau_score >= 800
    else:
        bureau_status = False
        multiple_bureau_check = False

    try:
        disbursed_dates = [x['disbursed_dt'] for x in tradelines]

        inquiry_dates = [x['inquiry_dates'] for x in bureau.accounts.li]
        pl_inq_dates = [x['inquiry_dates'] if x['inquiry_type'] in ['Personal Loan'] else None for x
                        in bureau.accounts.li]
    except:
        disbursed_dates = []
        inquiry_dates = []
        pl_inq_dates = []

    try:
        no_hit = check_no_hit_with_months(sourcing_dates, disbursed_dates, months=12)
    except:
        no_hit = None

    MV_NTC_Tag = additional_details.get('mvNTC')

    if (MV_NTC_Tag is True and no_hit is True) or bureau_status:
        pol_bureau_decision_status = True
    else:
        pol_bureau_decision_status = False

    cities_list = ['AHMEDABAD', 'DELHI', 'NEW DELHI', 'GURUGRAM', 'GURGAON', 'NOIDA', 'GAUTAM BUDDHA NAGAR',
                   'GHAZIABAD', 'FARIDABAD', 'MUMBAI', 'THANE', 'PUNE', 'BANGALORE', 'BENGALURU', 'HYDERABAD',
                   'CHENNAI', 'CHANDIGARH', 'JAIPUR']

    if loan_amount >= 100000:
        pol_city_match = True if ca_city.upper().strip() in cities_list else False
    else:
        pol_city_match = True

    final_emi = int(round(abs(float(np.pmt((requested_roi / 1200), tenure, -(loan_amount))))))

    if math.isnan(final_emi) or math.isinf(final_emi):
        final_emi = 0

    nbr_enquir_1_mth = int(inq_date_fun(pl_inq_dates, current_date, 1))

    if 0 <= nbr_enquir_1_mth <= 7:
        pol_enquir_1_mth = True
    else:
        pol_enquir_1_mth = False

    nbr_enquir_3_mth = int(inq_date_fun(inquiry_dates, current_date, 3))
    nbr_enquir_6_mth = int(inq_date_fun(inquiry_dates, current_date, 6))
    nbr_enquir_12_mth = int(inq_date_fun(inquiry_dates, current_date, 12))
    try:
        num_of_tradelines = bureau.accounts.tradeline.num_of_tradeline
        if num_of_tradelines is None:
            num_of_tradelines = 0
    except:
        num_of_tradelines = 0

    # ================================ TOP-UP POLICY ================================ #
    on_us_mob, on_us_current_date = on_us_mob_fun(additional_details.get('onUsLoanStartDate'))

    on_us_close_mob, on_us_current_date = on_us_mob_fun(additional_details.get('onUsLoanMaturityCloseDate'))

    top_up = additional_details.get("topUp", False)
    on_us_loan_amount = additional_details.get("onUsLoanAmount")
    try:
        on_us_dpd = int(additional_details.get("onUsDpd"))
    except:
        on_us_dpd = None

    try:
        on_us_count_clean_payments = int(additional_details.get("onUsCountCleanPayments"))
    except:
        on_us_count_clean_payments = None
    on_us_loan_status = str(additional_details.get("onUsLoanStatus")).lower()

    if on_us_count_clean_payments is not None:
        if top_up is True and (
                (on_us_loan_status in ['closed', 'close'] and on_us_dpd is not None and on_us_dpd == 0) or
                (on_us_loan_status in ['live',
                                       'active'] and on_us_mob is not None and on_us_mob >= 12 and on_us_count_clean_payments >= 6) or
                (on_us_loan_status in ['live',
                                       'active'] and on_us_mob is not None and 4 <= on_us_mob < 12 and on_us_count_clean_payments >= 3) or
                (on_us_loan_status in ['live',
                                       'active'] and on_us_mob is not None and on_us_mob < 4 and on_us_count_clean_payments >= 2)):
            mv_top_up_on_us_check = True
        else:
            mv_top_up_on_us_check = False
    else:
        mv_top_up_on_us_check = False

    mv_top_up_on_us_check = True if top_up is False else mv_top_up_on_us_check

    if top_up is True and on_us_loan_status in ['closed', 'close'] and on_us_close_mob is not None:
        verification_tag = False if on_us_close_mob > 3 else True
    elif top_up is True and on_us_loan_status in ['closed', 'close'] and on_us_close_mob is None:
        verification_tag = False
    else:
        verification_tag = True

    # ================================ TOP-UP POLICY END =========================================== #
    if int(num_of_tradelines) > 0 and not no_hit and bureau is not None and not is_ntc:
        type_of_xml = None
        if typeXML in ["MB", "MULTIBUREAU", "MULTI_BUREAU"]:
            tradelines = dpd_cleaner1(tradelines)
            type_of_xml = "MB"
        elif typeXML in ['EXPERIAN', 'EXP']:
            type_of_xml = "EXP"
            tradelines = dpd_cleaner_exp(tradelines)
        elif typeXML in ['CIBIL', 'CIBIL_JSON']:
            type_of_xml = "CIBIL_JSON"
            tradelines = dpd_cleaner_cibil(tradelines, current_date)

        for row in tradelines:
            row['unsecure'], row['secure'], row['other'] = type_accounts(row)
            row['reported_date'] = datetime.strptime(row['Date_Reported'].strip(),
                                                     "%Y-%m-%d").date() if row[
                                                                               'Date_Reported'] is not None else None
            row["source_date"] = sourcing_dates
            row["current_date"] = current_date
            row['sanctioned_amount'] = row['disbursed_amt']
            row["ownership_tag"] = ownership_tag_fun(row)
            row['mob'] = mob(row, current_date)
            row['max_mob_all'] = max_mob_all_fun_mv(row)
            row['mob_source_date'] = mob_source_date(row)
            row['mob_for_closed'] = mob_for_closed(row)
            row["live_account"] = live_account_cleaner(row["current_date"], row['close_dt'])
            row['ln_50K_6mth'] = ln_50K_6mth_fun(row)
            row["nbr_pl_12mths"] = nbr_pl_mths_fun_mv(row, 12)
            row['emi'] = emi_fun_mv(database.emi_for_acct_type_list, row)
            row["pol_only_gold_loan"] = pol_only_gold_loan_fun(row)
            row["pol_bl_loan"] = pol_bl_loan_fun(row)
            row["pol_only_guarantor"] = pol_only_guarantor_fun(row)
            row["nbr_pl_3mths"] = nbr_pl_mths_fun_mv(row, 3)
            row['nbr_pl_6mths'] = nbr_pl_mths_fun_mv(row, 6)
            row['pol_only_CD'] = pol_only_cd_fun(row)
            row['credit_card_limit'] = credit_card_limit(row)
            row['credit_limit'] = credit_limit(row)
            row["no_derog_24mths"] = no_derog_24mths_fun_new(row, type_of_xml)
            row["pol_no_derog_ever"] = pol_no_derog_ever_fun_new(row, type_of_xml)
            row["pol_90_plus_ever"] = pol_90_plus_ever_fun_new(row, type_of_xml)
            row["pol_60_plus_12mth"] = pol_dpd_plus_mths_fun_new(row, current_date, 60, 12,
                                                                 type_of_xml)
            row["pol_60_plus_6mth"] = pol_dpd_plus_mths_fun_new(row, current_date, 60, 6,
                                                                type_of_xml)
            row["pol_30_plus_3mth"] = pol_dpd_plus_mths_fun_new(row, current_date, 30, 3,
                                                                type_of_xml)
            row["pol_0_plus_current"] = pol_0_plus_current_fun_new(row, current_date, 0, 0,
                                                                   type_of_xml)
            row['live_indicator'] = live_indicator_fun(row)
            row['pol_90_plus_12mth'] = pol_dpd_plus_mths_fun_new(row, current_date, 90, 12,
                                                                 type_of_xml)

        live_account_list, pol_only_gold_loan_list, pol_bl_loan_list, pol_only_guarantor_list, \
            no_derog_24mths_list, pol_no_derog_ever_list, nbr_pl_3mths_list, nbr_pl_6mths_list, \
            pol_only_CD_list, pol_90_plus_ever_list, pol_60_plus_12mth_list, pol_60_plus_6mth_list, \
            pol_0_plus_current, nbr_pl_12mths_list, mob_list, max_mob_all_list, \
            pol_90_plus_12mths_list, pol_30_plus_3mths_list, pol_ln_50K_6mth_list = calculated_variables(tradelines)

        pol_bl_loan = any(pol_bl_loan_list)

        pol_only_gold_loan = all(pol_only_gold_loan_list)

        pol_only_guarantor = all(pol_only_guarantor_list)

        no_derog_24mths = True if int(sum(no_derog_24mths_list)) > 0 else False
        pol_no_90_plus_12mths = False if int(sum(pol_90_plus_12mths_list)) > 0 else True
        pol_no_30_plus_3mths = False if int(sum(pol_30_plus_3mths_list)) > 0 else True
        pol_ln_50K_6mth = False if int(sum(pol_ln_50K_6mth_list)) > 3 else True

        pol_no_derog_ever = True if int(sum(pol_no_derog_ever_list)) > 0 else False
        nbr_derog_ever = int(sum(pol_no_derog_ever_list))

        nbr_pl_6mths = int(sum(nbr_pl_6mths_list))
        pol_no_pl_6mths = False if int(sum(nbr_pl_6mths_list)) > 0 else True

        pol_only_CD = not all(pol_only_CD_list)

        pol_no_90_plus_ever = False if int(sum(pol_90_plus_ever_list)) > 0 else True

        pol_no_60_plus_12mths = False if int(sum(pol_60_plus_12mth_list)) > 0 else True
        pol_no_60_plus_6mths = False if int(sum(pol_60_plus_6mth_list)) > 0 else True

        pol_no_0_plus_current = False if int(sum(pol_0_plus_current)) > 0 else True

        nbr_pl_12mths_list = [row['disbursed_amt'] for row in tradelines if row['nbr_pl_12mths']
                              == 1]
        nbr_pl_12mths = int(len(nbr_pl_12mths_list)) if sum(nbr_pl_12mths_list) > 10000 \
            else 0
        try:
            cust_vintage = int(max(mob_list))
        except:
            cust_vintage = 0

        try:
            cust_bureau_vintage = int(max(max_mob_all_list))
        except:
            cust_bureau_vintage = 0

        pol_bureau_vintage_check = False if cust_bureau_vintage < 24 else True
        pol_cust_vintage_12_mths = False if cust_vintage <= 12 else True

        nbr_pl_3mths = int(sum(nbr_pl_3mths_list))
        pol_no_pl_3mths = False if int(nbr_pl_3mths) > 0 else True
        pol_no_2pl_12mths = False if nbr_pl_12mths > 2 else True
        nbr_90_plus_ever = int(sum(pol_90_plus_ever_list))
        nbr_60_plus_12mths = int(sum(pol_60_plus_12mth_list))
        nbr_30_plus_3mths = int(sum(pol_30_plus_3mths_list))
        nbr_0_plus_current = int(sum(pol_0_plus_current))
        enquiry_3_months = nbr_enquir_3_mth
        enquiry_12_months = nbr_enquir_12_mth
        enquiry_6_months = nbr_enquir_6_mth
        nbr_actv_pl_opened_in_12m = nbr_pl_12mths
        nbr_actv_pl_opened_in_6m = nbr_pl_6mths
        nbr_actv_pl_opened_in_3m = nbr_pl_3mths

        try:
            hl_amt_eligible_list = [row for row in tradelines if
                                    int(row['mob_source_date']) >= 24 and row['acct_type_id'] in [
                                        'A06', 'A07']]
        except:
            hl_amt_eligible_list = []

        if len(hl_amt_eligible_list) == 0:
            hl_amt_eligible_amt = 0.0
            hl_amt_eligible_df = []
        else:
            hl_amt_eligible_amt = max([x['disbursed_amt'] for x in hl_amt_eligible_list])
            hl_amt_eligible_df = [row for row in hl_amt_eligible_list if
                                  int(row['disbursed_amt']) == hl_amt_eligible_amt]

        try:
            hl_amt_eligible = float(0.75 * (float(hl_amt_eligible_amt) / 5.0))
        except:
            hl_amt_eligible = 0
            hl_amt_eligible_df = []

        try:
            pl_amt_eligible_list = [row for row in tradelines if
                                    int(row['max_mob_all']) >= 12 and row[
                                        'ownership_tag'] == True and row[
                                        'acct_type_id'] in [
                                        'A12']]
        except Exception as e:
            pl_amt_eligible_list = []
        if len(pl_amt_eligible_list) == 0:
            pl_amt_eligible_amt = 0.0
            pl_amt_eligible_df = []
        else:
            pl_amt_eligible_amt = max(
                [float(str(x['disbursed_amt']).replace(',', '')) for x in
                 pl_amt_eligible_list])
            pl_amt_eligible_df = [row for row in pl_amt_eligible_list if
                                  float(str(row['disbursed_amt']).replace(',', '')) == float(
                                      pl_amt_eligible_amt)]
        try:
            if nbr_pl_12mths in [0]:
                pl_amt_eligible = float(0.75 * pl_amt_eligible_amt)
            elif nbr_pl_12mths in [1]:
                pl_amt_eligible = float(0.65 * pl_amt_eligible_amt)
            elif nbr_pl_12mths in [2]:
                pl_amt_eligible = float(0.55 * pl_amt_eligible_amt)
            else:
                pl_amt_eligible = 0
                pl_amt_eligible_df = []
        except:
            pl_amt_eligible = 0
            pl_amt_eligible_df = []

        try:
            cc_amt_eligible_list = [row for row in tradelines if
                                    int(row['max_mob_all']) >= 12 and row[
                                        'ownership_tag'] == True and row[
                                        'acct_type_id'] in [
                                        'A15', 'A16', 'A17', 'A61'] and row['live_account'] in [
                                        1]]
        except:
            cc_amt_eligible_list = []

        if len(cc_amt_eligible_list) == 0:
            cc_current_bal = 0.0
            cc_credit_limit = 0.0
            cc_eligible_amt_list = []
            cc_amt_eligible = 0.0
        else:
            cc_current_bal = max(
                [float(str(x['current_bal']).replace(',', '')) for x in
                 cc_amt_eligible_list])

            for row in cc_amt_eligible_list:
                row['cc_amt_eligible'] = get_eligible_cc_amt(row)

            cc_amt_eligible = max([row['cc_amt_eligible'] for row in cc_amt_eligible_list])
            cc_eligible_amt_list = [row for row in cc_amt_eligible_list if
                                    float(str(row['cc_amt_eligible'])) == float(cc_amt_eligible)]

        try:
            cl_amt_eligible_list = [row for row in tradelines if
                                    int(row['max_mob_all']) >= 6 and row[
                                        'ownership_tag'] == True and row[
                                        'acct_type_id'] in [
                                        'A13']]
        except:
            cl_amt_eligible_list = []

        if len(cl_amt_eligible_list) == 0:
            cl_amt_eligible_amt = 0.0
            cl_eligible_amt_list = []
        else:
            cl_amt_eligible_amt = max(
                [float(str(x['disbursed_amt']).replace(',', '')) for x in
                 cl_amt_eligible_list])

        cl_eligible_amt_list = [row for row in cl_amt_eligible_list if
                                float(str(row['disbursed_amt']).replace(',', '')) == int(
                                    cl_amt_eligible_amt)]

        try:
            if nbr_pl_12mths == 0:
                cl_amt_eligible = float(2 * float(cl_amt_eligible_amt))
            elif nbr_pl_12mths == 1:
                cl_amt_eligible = float(1.5 * float(cl_amt_eligible_amt))
            elif nbr_pl_12mths == 2:
                cl_amt_eligible = float(cl_amt_eligible_amt)
            else:
                cl_amt_eligible = 0
                cl_eligible_amt_list = []
        except:
            cl_amt_eligible = 0
            cl_eligible_amt_list = []

        try:
            al_amt_eligible_list = [row for row in tradelines if
                                    row['ownership_tag'] == True and row['acct_type_id'] in [
                                        'A01']]
        except:
            al_amt_eligible_list = []

        try:
            al_max_mob = max([int(x['max_mob_all']) for x in al_amt_eligible_list])

            al_max_disbursed = max(
                [float(str(x['disbursed_amt']).replace(',', '')) for x in
                 al_amt_eligible_list])

            if len(al_amt_eligible_list) == 0:
                al_amt_eligible_amt = 0
                al_amt_eligible_df = []
            elif al_max_mob >= 48:
                al_amt_eligible_amt = 0.75 * float(al_max_disbursed)
                al_amt_eligible_df = [row for row in al_amt_eligible_list if
                                      float(str(row['disbursed_amt']).replace(',', '')) == float(
                                          al_max_disbursed)]
            elif al_max_mob >= 24:
                al_amt_eligible_amt = 0.65 * float(al_max_disbursed)
                al_amt_eligible_df = [row for row in al_amt_eligible_list if
                                      float(str(row['disbursed_amt']).replace(',', '')) == float(
                                          al_max_disbursed)]
            elif al_max_mob >= 12:
                al_amt_eligible_amt = 0.55 * float(al_max_disbursed)
                al_amt_eligible_df = [row for row in al_amt_eligible_list if
                                      float(str(row['disbursed_amt']).replace(',', '')) == float(
                                          al_max_disbursed)]
            else:
                al_amt_eligible_amt = 0
                al_amt_eligible_df = []
        except:
            al_amt_eligible_amt = 0
            al_amt_eligible_df = []

        max_eligibility = float(
            max(hl_amt_eligible, pl_amt_eligible, cc_amt_eligible, cl_amt_eligible,
                al_amt_eligible_amt))

        max_amount_dict = {'OTHER': 0.0, 'HL': hl_amt_eligible,
                           'CC': cc_amt_eligible, 'CL': cl_amt_eligible,
                           'PL': pl_amt_eligible, 'AL': al_amt_eligible_amt}
        final_pf = 0
        str_final = max(max_amount_dict.items(), key=lambda i: i[1])

        final_tradeline_name = str_final[0].upper()

        final_eligibility_amount = 0
        final_roi = 0
        final_tenure = 0
        final_tradeline_ln_amt = 0
        final_tradeline_util = 0
        final_tradeline_vintage = 0

        if final_tradeline_name in ['HL']:
            if len(hl_amt_eligible_df) == 0:
                final_tradeline_util = 0.0
                final_tradeline_vintage = 0
                final_tradeline_ln_amt = 0
            else:
                try:
                    final_tradeline_util = hl_amt_eligible_df[0]['disbursed_amt'] and (float(
                        hl_amt_eligible_df[0]['current_bal']) / float(
                        hl_amt_eligible_df[0]['disbursed_amt'])) * 100 or 0
                except:
                    final_tradeline_util = 0
                final_tradeline_vintage = int(hl_amt_eligible_df[0]['max_mob_all'])

                final_tradeline_ln_amt = float(
                    str(hl_amt_eligible_df[0]['disbursed_amt']).replace(',', ''))

            if 100000 <= hl_amt_eligible <= 1000000:
                final_eligibility_amount = float(hl_amt_eligible)
            elif hl_amt_eligible > 1000000:
                final_eligibility_amount = 1000000
            elif hl_amt_eligible < 50000:
                final_eligibility_amount = 0

        elif final_tradeline_name == 'PL':
            if len(pl_amt_eligible_df) == 0:
                final_tradeline_util = 0.0
                final_tradeline_vintage = 0
                final_tradeline_ln_amt = 0
            else:
                try:
                    final_tradeline_util = pl_amt_eligible_df[0]['disbursed_amt'] and (float(
                        pl_amt_eligible_df[0]['current_bal']) / float(
                        pl_amt_eligible_df[0]['disbursed_amt'])) * 100 or 0
                except:
                    final_tradeline_util = 0
                final_tradeline_vintage = int(pl_amt_eligible_df[0]['max_mob_all'])

                final_tradeline_ln_amt = float(
                    str(pl_amt_eligible_df[0]['disbursed_amt']).replace(',', ''))

            if 100000 <= pl_amt_eligible <= 750000:
                final_eligibility_amount = float(pl_amt_eligible)
            elif pl_amt_eligible > 750000:
                final_eligibility_amount = 750000
            elif pl_amt_eligible < 100000:
                final_eligibility_amount = 0

        elif final_tradeline_name == 'CC':
            if len(cc_eligible_amt_list) == 0:
                final_tradeline_util = 0.0
                final_tradeline_vintage = 0
                final_tradeline_ln_amt = 0
            else:
                try:
                    final_tradeline_util = cc_eligible_amt_list[0]['disbursed_amt'] and (float(
                        cc_eligible_amt_list[0]['current_bal']) / float(
                        cc_eligible_amt_list[0]['disbursed_amt'])) * 100 or 0
                except:
                    final_tradeline_util = 0
                final_tradeline_vintage = int(cc_eligible_amt_list[0]['max_mob_all'])

                final_tradeline_ln_amt = float(
                    str(cc_eligible_amt_list[0]['disbursed_amt']).replace(',', ''))

            if 100000 <= cc_amt_eligible <= 500000:
                final_eligibility_amount = float(cc_amt_eligible)
            elif cc_amt_eligible > 500000:
                final_eligibility_amount = 500000
            elif cc_amt_eligible < 50000:
                final_eligibility_amount = 0

        elif final_tradeline_name == 'CL':
            if len(cl_eligible_amt_list) == 0:
                final_tradeline_util = 0.0
                final_tradeline_vintage = 0
                final_tradeline_ln_amt = 0
            else:
                try:
                    final_tradeline_util = cl_eligible_amt_list[0]['disbursed_amt'] and (float(
                        cl_eligible_amt_list[0]['current_bal']) / float(
                        cl_eligible_amt_list[0]['disbursed_amt'])) * 100 or 0
                except:
                    final_tradeline_util = 0
                final_tradeline_vintage = int(cl_eligible_amt_list[0]['max_mob_all'])

                final_tradeline_ln_amt = float(
                    str(cl_eligible_amt_list[0]['disbursed_amt']).replace(',', ''))

            if 100000 <= cl_amt_eligible <= 250000:
                final_eligibility_amount = float(cl_amt_eligible)
            elif cl_amt_eligible > 250000:
                final_eligibility_amount = 250000
            elif cl_amt_eligible < 50000:
                final_eligibility_amount = 0

        elif final_tradeline_name == 'AL':
            if len(al_amt_eligible_df) == 0:
                final_tradeline_util = 0.0
                final_tradeline_vintage = 0
                final_tradeline_ln_amt = 0
            else:
                try:
                    final_tradeline_util = al_amt_eligible_df[0]['disbursed_amt'] and (float(
                        al_amt_eligible_df[0]['current_bal']) / float(
                        al_amt_eligible_df[0]['disbursed_amt'])) * 100 or 0
                except:
                    final_tradeline_util = 0
                final_tradeline_vintage = int(al_amt_eligible_df[0]['max_mob_all'])

                final_tradeline_ln_amt = float(
                    str(al_amt_eligible_df[0]['disbursed_amt']).replace(',', ''))

            if 100000 <= al_amt_eligible_amt <= 500000:
                final_eligibility_amount = float(al_amt_eligible_amt)
            elif al_amt_eligible_amt > 500000:
                final_eligibility_amount = 500000
            elif al_amt_eligible_amt < 50000:
                final_eligibility_amount = 0

        if final_eligibility_amount > 50000:
            loan_eligibilty_decision = "Accept"
        else:
            loan_eligibilty_decision = "Decline"

        if final_eligibility_amount > 500000:
            offer_greater_5lakhs = "P1"
        else:
            offer_greater_5lakhs = "P2"

        final_eligibility_emi = (final_eligibility_amount and requested_roi and tenure) and \
                                float(round(abs(float(np.pmt((requested_roi / 1200), tenure,
                                                             -final_eligibility_amount))),
                                            2)) or 0
        nbr_total_pl_list = [row for row in tradelines if
                             row['ownership_tag'] == True and row['acct_type_id'] in ['A12']]

        if len(nbr_total_pl_list) == 0:
            nbr_total_pl = 0
        else:
            nbr_total_pl = sum([x['ownership_tag'] for x in nbr_total_pl_list])

        nbr_live_pl_list = [row for row in tradelines if
                            row['ownership_tag'] == True and row['acct_type_id'] in ['A12'] and
                            row[
                                'live_account'] in [1]]

        if len(nbr_live_pl_list) == 0:
            nbr_live_pl = 0
        else:
            nbr_live_pl = sum([x['live_account'] for x in nbr_live_pl_list])

        if len(nbr_live_pl_list) == 0:
            total_pl_exposure = 0
        else:
            total_pl_exposure = sum(
                [float(str(x['disbursed_amt']).replace(',', '')) for x in
                 nbr_live_pl_list])
        active_pl_exposure = total_pl_exposure

        secured_exposure_list = [row for row in tradelines if
                                 row['secure'] in [1] and row['ownership_tag'] == True and row[
                                     'live_account'] in [1]]

        if len(secured_exposure_list) == 0:
            secured_exposure = 0
        else:
            secured_exposure = sum(
                [float(str(x['disbursed_amt']).replace(',', '')) for x in
                 secured_exposure_list])

        unsecured_exposure_list = [row for row in tradelines if
                                   row['unsecure'] in [1] and row['ownership_tag'] == True and
                                   row[
                                       'live_account'] in [
                                       1]]

        if len(unsecured_exposure_list) == 0:
            unsecured_exposure = 0
        else:
            unsecured_exposure = sum(
                [float(str(x['disbursed_amt']).replace(',', '')) for x in
                 unsecured_exposure_list])

        num_individual_tradelines = [row for row in tradelines if row['ownership_tag'] == True]

        if len(num_individual_tradelines) == 0:
            num_tradelines = 0
        else:
            num_tradelines = sum([x['ownership_tag'] for x in num_individual_tradelines])

        total_current_bal = num_individual_tradelines
        if len(total_current_bal) == 0:
            total_current_bal = 0
        else:
            try:
                total_current_bal = sum(
                    [float(str(x['current_bal']).replace(',', '')) for x in total_current_bal])
            except:
                total_current_bal = 0

        total_exposure_list = num_individual_tradelines
        if len(total_exposure_list) == 0:
            total_exposure = 0
        else:
            total_exposure = sum(
                [float(str(x['disbursed_amt']).replace(',', '')) for x in total_exposure_list])

        nbr_actv_act_opened_in_12m_list = [row for row in tradelines if
                                           int(row['max_mob_all']) >= 12 and row[
                                               'ownership_tag'] == True and row[
                                               'live_account'] in [1]]

        if len(nbr_actv_act_opened_in_12m_list) == 0:
            nbr_actv_act_opened_in_12m = 0
        else:
            nbr_actv_act_opened_in_12m = sum(
                [int(x['live_account']) for x in nbr_actv_act_opened_in_12m_list])

        nbr_actv_act_opened_in_6m_list = [row for row in tradelines if
                                          int(row['max_mob_all']) >= 6 and row[
                                              'ownership_tag'] == True and
                                          row['live_account'] in [
                                              1]]

        if len(nbr_actv_act_opened_in_6m_list) == 0:
            nbr_actv_act_opened_in_6m = 0
        else:
            nbr_actv_act_opened_in_6m = sum(
                [int(x['live_account']) for x in nbr_actv_act_opened_in_6m_list])

        nbr_actv_act_opened_in_3m_list = [row for row in tradelines if
                                          int(row['max_mob_all']) >= 3 and row[
                                              'ownership_tag'] == True and
                                          row['live_account'] in [
                                              1]]

        if len(nbr_actv_act_opened_in_3m_list) == 0:
            nbr_actv_act_opened_in_3m = 0
        else:
            nbr_actv_act_opened_in_3m = sum(
                [int(x['live_account']) for x in nbr_actv_act_opened_in_3m_list])

        if (((bureau_score not in [None]) and (final_tradeline_name in ["HL"] and multiple_bureau_check)) or (
                final_tradeline_name in ["PL"] and cust_bureau_vintage >= 36) or (
                final_tradeline_name in ["CC"] and final_tradeline_vintage >= 24)):
            segment = "A1"
        elif (((bureau_score not in [None]) and (
                final_tradeline_name in ["HL"] and multiple_bureau_check is False)) or (
                      final_tradeline_name in ["PL"] and cust_bureau_vintage < 36) or (
                      final_tradeline_name in ["AL"] and final_tradeline_vintage >= 36)):
            segment = "A2"
        elif ((final_tradeline_name in ["CL"] and ca_city.lower().strip() not in ["mumbai", "hyderabad", "thane",
                                                                                  "pune"]) or (
                      final_tradeline_name in ["CC"] and final_tradeline_vintage < 24)):
            segment = "A3"
        elif ((final_tradeline_name in ["CL"] and ca_city.lower().strip() in ["mumbai", "hyderabad", "thane",
                                                                              "pune"]) or (
                      final_tradeline_name in ["AL"] and final_tradeline_vintage < 36)):
            segment = "A4"
        else:
            segment = "others"

        pol_segment_AL_rule = False if final_tradeline_name in [
            "AL"] and final_tradeline_ln_amt < 300000 else True
        pol_segment_CL_rule = False if final_tradeline_name in ["CL"] else True

        current_obligation_list = [row for row in tradelines if row['live_account'] == 1]
        if len(current_obligation_list) == 0:
            current_obligation = 0
        else:
            current_obligation = sum([row['emi'] for row in current_obligation_list])

        try:
            dbr = current_obligation / monthly_income
        except:
            dbr = 1.0

        if dbr <= 0.7:
            pol_dbr_check = True
        else:
            pol_dbr_check = False

        if not any(live_account_list) and min(mob_list) > 6:
            pol_no_inactive_acc = False
        else:
            pol_no_inactive_acc = True

        if final_tradeline_name in ['AL'] and (
                'A41' in [x['acct_type_id'] for x in tradelines]):
            pol_no_usedcar_accs = False
        else:
            pol_no_usedcar_accs = True

        bureau_dedupe = False if any(['ac' in str(i).lower() for i in [row['acct_number'] for row in tradelines]]) and \
                                 any(['clix' in str(i).lower() for i in
                                      [row['member_name'] for row in inquiry_data]]) else True

        if scheme_name in ["CATA", "CATB", "GROWTH"]:
            if pol_enquir_1_mth == True and \
                    pol_loan_amt_check == True and \
                    pol_age_match == True and \
                    pol_bureau_decision_status == True and \
                    no_derog_24mths == False and \
                    pol_no_90_plus_12mths == True and \
                    pol_no_30_plus_3mths == True and \
                    pol_no_0_plus_current == True and \
                    bureau_dedupe == True and \
                    pol_ln_50K_6mth == True and \
                    mv_top_up_on_us_check is True and \
                    pol_roi_check is True and \
                    pol_tenure_check is True and \
                    pol_higher_eligibility_check is True:
                # pol_negative_area_match == False and \
                # verification_tag is True
                pol_decision_status = "Accept"
            else:
                pol_decision_status = "Decline"
        else:
            if pol_enquir_1_mth == True and \
                    pol_loan_amt_check == True and \
                    pol_age_match == True and \
                    pol_bureau_decision_status == True and \
                    pol_income_match == True and \
                    no_derog_24mths == False and \
                    pol_no_90_plus_12mths == True and \
                    pol_no_30_plus_3mths == True and \
                    pol_no_0_plus_current == True and \
                    bureau_dedupe == True and \
                    pol_ln_50K_6mth == True and \
                    mv_top_up_on_us_check is True and \
                    pol_roi_check is True and \
                    pol_tenure_check is True and \
                    pol_higher_eligibility_check is True:
                # pol_negative_area_match == False and \
                pol_decision_status = "Accept"
            else:
                pol_decision_status = "Decline"

        if pol_decision_status in ["Decline"]:
            rejection_reason = rejection_reasons(no_hit, scheme_name, pol_enquir_1_mth, pol_loan_amt_check,
                                                 pol_negative_area_match, pol_age_match,
                                                 pol_bureau_decision_status, pol_income_match, no_derog_24mths,
                                                 pol_no_90_plus_12mths, pol_no_30_plus_3mths,
                                                 pol_no_0_plus_current, bureau_dedupe, pol_ln_50K_6mth,
                                                 mv_top_up_on_us_check, verification_tag, pol_roi_check,
                                                 pol_tenure_check, pol_higher_eligibility_check)
        else:
            rejection_reason = None

        if pol_negative_area_match is False and pol_age_match is True and pol_decision_status in [
            "Accept"] and loan_eligibilty_decision in ["Accept"]:
            final_decision = True
        else:
            final_decision = False

        resp = {
            "active_pl_exposure": float("{0:.2f}".format(active_pl_exposure)),
            "address_match_bureau": pol_city_match,
            "al_amt_eligible": float("{0:.2f}".format(al_amt_eligible_amt)),
            "sourcing_date": str(sourcing_dates),
            "age": age,
            "bureau_name": bureau_name,
            "bureau_score": bureau_score,
            "bureau_status": bureau_status,
            "bureau_dedupe": bureau_dedupe,
            "bureau_message": bureau_message,
            "is_ntc": is_ntc,
            "primary_ntc": is_ntc,
            "cc_amt_eligible": float("{0:.2f}".format(cc_amt_eligible)),
            "channel": channel,
            "cl_amt_eligible": float("{0:.2f}".format(cl_amt_eligible)),
            "current_obligation": float("{0:.2f}".format(current_obligation)),
            "cust_bureau_vintage": cust_bureau_vintage,
            "pol_enquir_1_mth": pol_enquir_1_mth,
            "enquiry_3_months": nbr_enquir_3_mth,
            "enquiry_6_months": nbr_enquir_6_mth,
            "enquiry_12_months": nbr_enquir_12_mth,
            "final_decision": final_decision,
            "final_eligibility_amount": float("{0:.2f}".format(final_eligibility_amount)),
            "final_loan_amount": int(final_loan_amount),
            "final_emi": final_emi,
            "final_pf": int(requested_pf),
            "final_roi": float("{0:.2f}".format(requested_roi)),
            "final_tenure": tenure,
            "eligible_emi": final_eligibility_emi,
            "eligible_pf": final_pf,
            "eligible_roi": final_roi,
            "eligible_tenure": final_tenure,
            "final_tradeline_ln_amt": float("{0:.2f}".format(final_tradeline_ln_amt)),
            "final_tradeline_name": final_tradeline_name,
            "final_tradeline_util": float("{0:.2f}".format(final_tradeline_util)),
            "final_tradeline_vintage": final_tradeline_vintage,
            "hl_amt_eligible": float("{0:.2f}".format(hl_amt_eligible)),
            "loan_eligibilty_decision": loan_eligibilty_decision,
            "loanApplicationId": application_id,
            "phone_number": mobile,
            "max_eligibility": float("{0:.2f}".format(max_eligibility)),
            "monthly_income": float("{0:.2f}".format(monthly_income)),
            "nbr_enquir_1_mth": nbr_enquir_1_mth,
            "pol_ln_50K_6mth": pol_ln_50K_6mth,
            "nbr_0_plus_current": nbr_0_plus_current,
            "nbr_30_plus_3mths": nbr_30_plus_3mths,
            "nbr_60_plus_12mths": nbr_60_plus_12mths,
            "nbr_90_plus_ever": nbr_90_plus_ever,
            "pol_no_90_plus_12mths": pol_no_90_plus_12mths,
            "nbr_actv_act_opened_in_3m": nbr_actv_act_opened_in_3m,
            "nbr_actv_act_opened_in_6m": nbr_actv_act_opened_in_6m,
            "nbr_actv_act_opened_in_12m": nbr_actv_act_opened_in_12m,
            "nbr_actv_pl_opened_in_3m": nbr_actv_pl_opened_in_3m,
            "nbr_actv_pl_opened_in_6m": nbr_actv_pl_opened_in_6m,
            "nbr_actv_pl_opened_in_12m": nbr_actv_pl_opened_in_12m,
            "nbr_derog_ever": nbr_derog_ever,
            "nbr_live_pl": nbr_live_pl,
            "nbr_pl_3mths": nbr_pl_3mths,
            "nbr_pl_12mths": nbr_pl_12mths,
            "nbr_total_pl": nbr_total_pl,
            "no_derog_24mths": no_derog_24mths,
            "no_hit_tag": no_hit,
            "num_tradelines": num_of_tradelines,
            "offer_greater_5lakhs": offer_greater_5lakhs,
            "pincode_map_bureau": pincode_map_bureau,
            "pol_loan_amt_check": pol_loan_amt_check,
            "pl_amt_eligible": float("{0:.2f}".format(pl_amt_eligible)),
            "pol_tenure_check": pol_tenure_check,
            "pol_roi_check": pol_roi_check,
            "pol_age_match": pol_age_match,
            "pol_bl_loan": pol_bl_loan,
            "pol_bureau_decision_status": pol_bureau_decision_status,
            "pol_city_match": pol_city_match,
            "pol_cust_vintage_12_mths": pol_cust_vintage_12_mths,
            "pol_dbr_check": pol_dbr_check,
            "pol_decision_status": pol_decision_status,
            "pol_income_match": pol_income_match,
            "pol_negative_area_match": pol_negative_area_match,
            "pol_no_0_plus_current": pol_no_0_plus_current,
            "pol_no_2pl_12mths": pol_no_2pl_12mths,
            "pol_no_30_plus_3mths": pol_no_30_plus_3mths,
            "pol_no_60_plus_12mths": pol_no_60_plus_12mths,
            "pol_no_60_plus_6mths": pol_no_60_plus_6mths,
            "pol_no_90_plus_ever": pol_no_90_plus_ever,
            "pol_no_derog_ever": pol_no_derog_ever,
            "pol_no_pl_3mths": pol_no_pl_3mths,
            "pol_only_CD": pol_only_CD,
            "pol_only_gold_loan": pol_only_gold_loan,
            "pol_only_guarantor": pol_only_guarantor,
            "pol_segment_AL_rule": pol_segment_AL_rule,
            "pol_segment_CL_rule": pol_segment_CL_rule,
            "secured_exposure": secured_exposure,
            "segment": segment,
            "scheme_name": scheme_name,
            "total_current_bal": float("{0:.2f}".format(total_current_bal)),
            "total_exposure": float("{0:.2f}".format(total_exposure)),
            "total_pl_exposure": float("{0:.2f}".format(total_pl_exposure)),
            "unsecured_exposure": float("{0:.2f}".format(unsecured_exposure)),
            "rejection_reason": rejection_reason,
            "pol_no_inactive_acc": pol_no_inactive_acc,
            "pol_no_usedcar_accs": pol_no_usedcar_accs,
            "reject_review_tag": False,
            "banking_trigger": True,
            "ev_perfios_match": 'Decline',
            "ev_comp_bureau_match": False,
            'policy_type': 'PRIME',
            "on_us_loan_amount": on_us_loan_amount,
            "top_up": top_up,
            "on_us_mob": on_us_mob,
            "on_us_close_mob": on_us_close_mob,
            "on_us_current_date": str(on_us_current_date),
            "on_us_loan_status": on_us_loan_status,
            "on_us_dpd": on_us_dpd,
            "on_us_count_clean_payments": on_us_count_clean_payments,
            "pol_topup_on_us_check": mv_top_up_on_us_check,
            "pol_verification_tag": verification_tag,
            "type_xml": typeXML,
            "transaction_flag": "bureau",
            "offered_loan_amount": offered_loan_amount,
            "tenure_factor": tenure_factor,
            "end_obligation": end_obligation,
            "risk_band_value": risk_band_value,
            "scheme_priority_band": scheme_priority_band,
            "verified_income_band": verified_income_band,
            "scheme_priority_value": scheme_priority_value,
             "R2_value": R2_value,
            "S2_value": S2_value,
            "tenure_capped": tenure_capped,
            "loan_amount_cap_l1": round(loan_amount_cap_l1, 2),
            "emi_multiplier_cap": emi_multiplier_cap,
            "max_emi": round(max_emi, 2),
            "emi_1": emi_1,
            "emi_2": emi_2,
            "loan_multiplier_cap": loan_multiplier_cap,
            "loan_amount_on_roi_tenure_emi_l2": round(loan_amount_on_roi_tenure_emi_l2, 2),
            "max_loan_amount_l3": round(max_loan_amount_l3, 2),
            "rounded_minimum_loan_amount": round(rounded_minimum_loan_amount, 2),
            "calculated_loan_amount": round(calculated_loan_amount, 2),
            "pol_higher_eligibility_check": pol_higher_eligibility_check
        }
        return resp, tradelines, warnings
    else:
        ntc_reasons = []
        primary_ntc = is_ntc
        is_ntc = True
        if int(num_of_tradelines) <= 0 and bureau is not None:
            ntc_reasons.append("No accounts in bureau")

        if no_hit and bureau is not None:
            ntc_reasons.append("Max Bureau vintage is in 12 Months")

        if bureau is None:
            if "consumer record not found" in bureau_message:
                ntc_reasons.append("NTC case as consumer record not found")
            else:
                ntc_reasons.append("Alert: Invalid Bureau")
        else:
            if primary_ntc in [False, None] and not no_hit:
                ntc_reasons.append("Alert: Invalid Bureau")

        if scheme_name in ["CATA", "CATB", "GROWTH"]:
            if pol_loan_amt_check is True and \
                    pol_negative_area_match is False and \
                    pol_age_match is True and \
                    pol_roi_check is True and \
                    pol_tenure_check is True and \
                    pol_higher_eligibility_check is True and \
                    is_ntc is True:
                pol_decision_status = "Accept"
            else:
                pol_decision_status = "Decline"
        else:
            if pol_loan_amt_check is True and \
                    pol_negative_area_match is False and \
                    pol_age_match is True and \
                    pol_income_match is True and \
                    pol_roi_check is True and \
                    pol_tenure_check is True and \
                    pol_higher_eligibility_check is True and \
                    is_ntc is True:
                pol_decision_status = "Accept"
            else:
                pol_decision_status = "Decline"

        if pol_decision_status in ["Decline"]:
            rejection_reason = ntc_rejection_reasons(no_hit, scheme_name, pol_loan_amt_check,
                                                     pol_negative_area_match, pol_age_match, pol_income_match,
                                                     pol_roi_check, pol_tenure_check, pol_higher_eligibility_check)
        else:
            rejection_reason = None

        if pol_negative_area_match is False and pol_age_match is True and pol_decision_status in ["Accept"]:
            final_decision = True
        else:
            final_decision = False

        resp = {
            "loanApplicationId": application_id,
            "phone_number": mobile,
            "sourcing_date": str(sourcing_dates),
            "age": age,
            "active_pl_exposure": None,
            "address_match_bureau": None,
            "al_amt_eligible": None,
            "bureau_name": bureau_name,
            "bureau_score": bureau_score,
            "bureau_status": bureau_status,
            "bureau_dedupe": None,
            "pol_bureau_decision_status": pol_bureau_decision_status,
            "bureau_message": bureau_message,
            "ntc_reasons": ntc_reasons,
            "is_ntc": is_ntc,
            "primary_ntc": primary_ntc,
            "cc_amt_eligible": None,
            "channel": channel,
            "cl_amt_eligible": None,
            "current_obligation": None,
            "cust_bureau_vintage": None,
            "pol_enquir_1_mth": pol_enquir_1_mth,
            "enquiry_3_months": nbr_enquir_3_mth,
            "enquiry_6_months": nbr_enquir_6_mth,
            "enquiry_12_months": nbr_enquir_12_mth,
            "final_decision": final_decision,
            "final_eligibility_amount": None,
            "final_loan_amount": loan_amount,
            "final_emi": final_emi,
            "final_tenure": tenure,
            "final_pf": requested_pf,
            "final_roi": requested_roi,
            "eligible_emi": None,
            "eligible_pf": None,
            "eligible_roi": None,
            "eligible_tenure": None,
            "final_tradeline_ln_amt": None,
            "final_tradeline_name": None,
            "final_tradeline_util": None,
            "final_tradeline_vintage": None,
            "hl_amt_eligible": None,
            "pol_loan_amt_check": pol_loan_amt_check,
            "loan_eligibilty_decision": "Decline",
            "max_eligibility": None,
            "monthly_income": monthly_income,
            "nbr_0_plus_current": None,
            "nbr_30_plus_3mths": None,
            "nbr_60_plus_12mths": None,
            "nbr_90_plus_ever": None,
            "nbr_actv_act_opened_in_3m": None,
            "nbr_actv_act_opened_in_6m": None,
            "nbr_actv_act_opened_in_12m": None,
            "nbr_actv_pl_opened_in_3m": None,
            "nbr_actv_pl_opened_in_6m": None,
            "nbr_actv_pl_opened_in_12m": None,
            "nbr_derog_ever": None,
            "nbr_live_pl": None,
            "nbr_pl_3mths": None,
            "nbr_pl_12mths": None,
            "nbr_total_pl": None,
            "no_derog_36mths": None,
            "no_hit_tag": no_hit,
            "num_tradelines": num_of_tradelines,
            "offer_greater_5lakhs": None,
            "pincode_map_bureau": None,
            "pl_amt_eligible": None,
            "pol_age_match": pol_age_match,
            "pol_bl_loan": None,
            "pol_city_match": pol_city_match,
            "pol_cust_vintage_12_mths": None,
            "pol_decision_status": pol_decision_status,
            "pol_income_match": pol_income_match,
            "pol_negative_area_match": pol_negative_area_match,
            "pol_no_0_plus_current": None,
            "pol_no_2pl_12mths": None,
            "pol_no_30_plus_3mths": None,
            "pol_no_60_plus_12mths": None,
            "pol_no_90_plus_ever": None,
            "pol_no_derog_ever": None,
            "pol_no_pl_3mths": None,
            "pol_only_CD": None,
            "pol_only_gold_loan": None,
            "pol_only_guarantor": None,
            "pol_segment_AL_rule": None,
            "pol_segment_CL_rule": None,
            "secured_exposure": None,
            "segment": None,
            "total_current_bal": None,
            "total_exposure": None,
            "total_pl_exposure": None,
            "unsecured_exposure": None,
            "rejection_reason": rejection_reason,
            "pol_no_inactive_acc": None,
            "pol_no_usedcar_accs": None,
            "reject_review_tag": False,
            "banking_trigger": True,
            "ev_perfios_match": 'Decline',
            "ev_comp_bureau_match": False,
            'policy_type': 'PRIME',
            "on_us_loan_amount": on_us_loan_amount,
            "top_up": top_up,
            "on_us_mob": on_us_mob,
            "on_us_close_mob": on_us_close_mob,
            "on_us_current_date": str(on_us_current_date),
            "on_us_loan_status": on_us_loan_status,
            "on_us_dpd": on_us_dpd,
            "on_us_count_clean_payments": on_us_count_clean_payments,
            "pol_topup_on_us_check": mv_top_up_on_us_check,
            "pol_verification_tag": verification_tag,
            "pol_tenure_check": pol_tenure_check,
            "pol_roi_check": pol_roi_check,
            "nbr_enquir_1_mth": None,
            "no_derog_24mths": None,
            "pol_dbr_check": None,
            "pol_ln_50K_6mth": None,
            "pol_no_60_plus_6mths": None,
            "pol_no_90_plus_12mths": None,
            "scheme_name": scheme_name,
            "type_xml": typeXML,
            "transaction_flag": "ntc",
            "offered_loan_amount": offered_loan_amount,
            "tenure_factor": tenure_factor,
            "end_obligation": end_obligation,
            "risk_band_value": risk_band_value,
            "scheme_priority_band": scheme_priority_band,
            "verified_income_band": verified_income_band,
            "scheme_priority_value": scheme_priority_value,
            "R2_value": R2_value,
            "S2_value": S2_value,
            "tenure_capped": tenure_capped,
            "loan_amount_cap_l1": round(loan_amount_cap_l1, 2),
            "emi_multiplier_cap": emi_multiplier_cap,
            "max_emi": round(max_emi, 2),
            "emi_1": emi_1,
            "emi_2": emi_2,
            "loan_multiplier_cap": loan_multiplier_cap,
            "loan_amount_on_roi_tenure_emi_l2": round(loan_amount_on_roi_tenure_emi_l2, 2),
            "max_loan_amount_l3": round(max_loan_amount_l3, 2),
            "rounded_minimum_loan_amount": round(rounded_minimum_loan_amount, 2),
            "calculated_loan_amount": round(calculated_loan_amount, 2),
            "pol_higher_eligibility_check": pol_higher_eligibility_check
        }
        return resp, tradelines, warnings
