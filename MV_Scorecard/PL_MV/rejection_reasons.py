def rejection_reasons(no_hit, scheme_name, pol_enquir_1_mth, pol_loan_amt_check,
                                                 pol_negative_area_match, pol_age_match,
                                                 pol_bureau_decision_status, pol_income_match, no_derog_24mths,
                                                 pol_no_90_plus_12mths, pol_no_30_plus_3mths,
                                                 pol_no_0_plus_current, bureau_dedupe, pol_ln_50K_6mth,
                                                 mv_top_up_on_us_check, verification_tag, pol_roi_check, pol_tenure_check, pol_higher_eligibility_check):
    reasons = []

    if scheme_name not in ["CATA", "CATB", "GROWTH"]:
        if pol_income_match not in [True]:
            reasons.append('pol_income_match is False')

    if pol_enquir_1_mth not in [True]:
        reasons.append('pol_enquir_1_mth is False')

    if pol_loan_amt_check not in [True]:
        if scheme_name in ["GROWTH"]:
            reasons.append('final loan amount not in 5k-25k range')
        else:
            reasons.append('final loan amount not in 15k-5lac range')

    # if pol_negative_area_match not in [False]:
    #     reasons.append('pol_negative_area_match is True')

    if pol_age_match not in [True]:
        reasons.append('pol_age_match is False')

    if pol_bureau_decision_status not in [True]:
        reasons.append('pol_bureau_decision_status is False')

    if no_derog_24mths not in [False]:
        reasons.append('no_derog_24mths is True')

    if pol_no_90_plus_12mths not in [True]:
        reasons.append('pol_no_90_plus_12mths is False')

    # if pol_no_60_plus_12mths not in [True]:
    #     reasons.append('pol_no_60_plus_12mths is False')

    if pol_no_30_plus_3mths not in [True]:
        reasons.append('pol_no_30_plus_3mths is False')

    if pol_no_0_plus_current not in [True]:
        reasons.append('pol_no_0_plus_current is False')

    if bureau_dedupe not in [True]:
        reasons.append('bureau_dedupe is False')

    if pol_ln_50K_6mth not in [True]:
        reasons.append("pol_ln_50K_6mth is False")

    if mv_top_up_on_us_check not in [True]:
        reasons.append('mv_pol_topup_on_us_check is False')

    if verification_tag not in [True]:
        reasons.append("verification_tag is False")

    if pol_tenure_check not in [True]:
        reasons.append("pol_tenure_check is False")

    if pol_roi_check not in [True]:
        reasons.append("pol_roi_check is False")

    if pol_higher_eligibility_check not in [True]:
        reasons.append("not eligible for higher loan amount")

    return reasons


def ntc_rejection_reasons(no_hit, scheme_name, pol_loan_amt_check, pol_negative_area_match, pol_age_match, pol_income_match, pol_roi_check, pol_tenure_check, pol_higher_eligibility_check):
    reasons = []
    if scheme_name not in ["CATA", "CATB", "GROWTH"]:
        if pol_income_match not in [True]:
            reasons.append('pol_income_match is False')

    if pol_loan_amt_check not in [True]:
        reasons.append('pol_loan_amt_check is False')

    if pol_negative_area_match not in [False]:
        reasons.append('pol_negative_area_match is True')

    if pol_age_match not in [True]:
        reasons.append('pol_age_match is False')

    if pol_tenure_check not in [True]:
        reasons.append("pol_tenure_check is False")

    if pol_roi_check not in [True]:
        reasons.append("pol_roi_check is False")

    if pol_higher_eligibility_check not in [True]:
        reasons.append("not eligible for higher loan amount")

    return reasons
