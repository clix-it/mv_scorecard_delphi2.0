import json, os
import operator
import psycopg2
import logging
import boto3
from getBureau import *
from config.connection import *
from functions.operations import *
from functions.response import *
from common_functions import *
from functions.conf_functions import *
from PL_MV.mv_scorecard import *
from TL_vars import *
import sentry_sdk
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration

logger = logging.getLogger()
logger.setLevel(logging.INFO)

if os.environ["is_sentry"].lower() in ['true']:
    sentry_sdk.init(dsn=os.environ["sentry_dsn"], integrations=[AwsLambdaIntegration()], environment=os.environ["env"])

logger.info("Calling Secret manger...")
try:
    client = boto3.client('lambda')
    presponse = client.invoke(
        FunctionName=os.environ['GET_PASSWORD_ARN'],
        InvocationType='RequestResponse',
        Payload=json.dumps({"queryStringParameters": {"secret_name": "delphi2_psql"}})
    )
    precords = json.load(presponse['Payload'])
    password = dict(precords).get('body').get('password')
except:
    password = os.environ['password']

# os.environ['env'] = 'local'
if os.environ['env'] in ['local']:
    password = os.environ['password']

try:
    # connect to the PostgreSQL server
    logger.info("Creating connections...")
    conn = psycopg2.connect(host=os.environ['host'], user=os.environ['user'], password=password,
                            database=os.environ['database'], port=os.environ['port'])
    logger.info("Connection successful")
    error = None
except Exception as e:
    logger.error(e)
    error = 'Connection Failed'
    logger.error(error)

try:
    # connect to the PostgreSQL Routing_Layer
    logger.info("Creating connections1...")
    conn1 = psycopg2.connect(host=os.environ['host'], user=os.environ['user'], password=password,
                             database=os.environ['database1'], port=os.environ['port'])
    logger.info("Connection1 successful")
    routing_layer_cursor = conn1.cursor()
    error = None
except Exception as e:
    logger.error(e)
    routing_layer_cursor = None
    error = 'Connection Failed1'
    logger.error(error)


def get_scorecard(event, context):
    logger.info(f"Loading {os.environ['env']} MV_Scorecard")
    # if os.environ['env'] not in ["UAT", "Production", "PROD"]:
    # event = open('functions/bureau.json')
    # event = json.load(event)

    warnings = {}

    try:
        applicationStage = event["report"]["stage"]["applicationStage"].lower()
    except:
        applicationStage = 'none'

    try:
        application_id = event["applicationId"]
    except:
        application_id = None

    query = "select status from req_resp_scorecards where loan_app_id = '{}' order by id desc;".format(application_id)
    try:
        routing_layer_cursor.execute(query)
        data = routing_layer_cursor.fetchall()
    except:
        routing_layer_cursor.execute("rollback")
        response = {"pol_decision_status": "Decline",
                    "rejection_reason": ["The application has invalid data"]}
        return response_json(event, response, None, warnings, applicationStage)

    decision_status = [status for status in data]

    if any(str(status[0]).lower() in ["reject", "decline", "refer_to_credit", "refer"] for status in data):
        response = {"pol_decision_status": "Decline",
                    "rejection_reason": [f"The application has already been rejected or refer at the previous stage"]}
        return response_json(event, response, None, warnings, applicationStage)

    if applicationStage in [None, 'none']:
        warnings["applicationStage"] = 'applicationStage is not present in json'
        return warning_resp_json(event, os.environ['env'], warnings)

    if str(applicationStage).lower() in ['hunter']:
        hunter_response, warnings = get_hunter_decision(event, conn1)
        return response_json(event, hunter_response, None, warnings, applicationStage)

    if str(applicationStage).lower() in ['posidex']:
        report = event.get('report')
        partner = event.get('partner')
        posidex_data = report.get('posidexData')
        posidex_response = posidex_data.get('posidexResp', {})
        response_code = str(posidex_response.get('RESPONSE_CODE'))
        matched_customer_details_list = posidex_response.get('MATCHED_CUSTOMER_DETAILS', [])
        match_criteria_list = [matched_customer_details_row.get('MATCH_CRITERIA_LIST', [{}])[0] for
                               matched_customer_details_row
                               in matched_customer_details_list if matched_customer_details_list is not None and
                               len(matched_customer_details_list) > 0]

        if posidex_data is None or not posidex_data:
            warnings["posidexData"] = 'posidexData is not present'
            return warning_resp_json(event, os.environ['env'], warnings)
        elif posidex_response is None or not posidex_response:
            warnings["posidexData"] = 'posidexResp is not present'
            return warning_resp_json(event, os.environ['env'], warnings)
        elif response_code not in ['200', '201']:
            warnings["posidexData"] = 'error in posidexData'
            return warning_resp_json(event, os.environ['env'], warnings)
        # elif matched_customer_details_list is None or not matched_customer_details_list:
        #     warnings["posidexData"] = 'MATCHED_CUSTOMER_DETAILS is not present'
        #     return warning_resp_json(event, os.environ['env'], warnings)
        # elif not all(match_criteria_list) or not match_criteria_list:
        #     warnings["posidexData"] = 'MATCH_CRITERIA_LIST is not present'
        #     return warning_resp_json(event, os.environ['env'], warnings)
        posidex_response, warnings = get_posidex_decision(posidex_data, partner)
        return response_json(event, posidex_response, None, warnings, applicationStage)

    applicant = event['report']['applicant']
    qde = applicant['qde']

    if error is not None:
        warnings["DataBase Info"] = error
        return warning_resp_json(event, os.environ['env'], warnings)

    database = Database(conn)
    # db_list = database.list_data
    # db_data = GetValues(db_list).db

    try:
        bureauData = event["report"]["applicant"]["bureauData"]
    except:
        bureauData = None

    if bureauData is None:
        warnings["bureauData"] = 'Bureau Data is not present in xml'
        return warning_resp_json(event, os.environ['env'], warnings)

    typeXML = str(event["report"]["applicant"]["bureauData"]["type"]).upper()
    xml = event["report"]["applicant"]["bureauData"]["bureauXML"]
    try:
        bureau_json = event["report"]["applicant"]["bureauData"]["bureauJson"]
    except Exception:
        bureau_json = {}
    bureau_status = bool(event["report"]["applicant"].get("bureauData", {}).get("status"))
    bureau_message = str(event["report"]["applicant"]["bureauData"]["message"]).lower().replace(".", "")

    if "consumer record not found" in bureau_message:
        bureau = None
        is_ntc = True
    else:
        is_ntc = False
        if typeXML not in ["MB", "MULTIBUREAU", "MULTI_BUREAU", 'EXPERIAN', 'EXP', 'CIBIL_JSON']:
            warnings["typeXML"] = 'Not valid value: ' + str(typeXML)

        if typeXML == 'CIBIL_JSON':
            xml = bureau_json
            if bureau_json in [None, "", {}]:
                warnings["bureauData"] = "Invalid Bureau JSON"

        if bool(warnings):
            return warning_resp_json(event, os.environ['env'], warnings)

        try:
            bureau = Parser(typeXML, xml).bureau
            logger.info("Parser Run Successfully")
            # db_data = GetValues(db_list, bureau).db
        except Exception:
            warnings["bureauData"] = 'Invalid Bureau XML'
            return warning_resp_json(event, os.environ['env'], warnings)

    pq_consumer_bureau_data, tradeline, warnings = pq_consumer_bureau(event, bureau,
                                                                      database, conn, typeXML,
                                                                      applicationStage, bureau_message, is_ntc)

    if bureau is not None:
        tl_vars_resp = TL_vars(database.mb_account_type_id_dict, bureau, tradeline)
    else:
        tl_vars_resp = []

    logger.info(pq_consumer_bureau_data)

    return response_json(event, pq_consumer_bureau_data, tl_vars_resp, warnings, applicationStage)
