from functions.db_variables import *
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class GetValues:
    def __init__(self, db_list=None, bureau=None):
        self.db_list = db_list
        self.bureau = bureau
        self.db = DBVariables()
        self.set_values()

    def set_values(self):
        for i in self.db_list:
            if i[2] == "BS1":
                self.db.score_ops = i[3]
                self.db.score_value = i[4]
            elif i[2] == "age":
                self.db.age_ops = i[3]
                self.db.age_value = i[4]
            elif i[2] == "income":
                self.db.income_ops = i[3]
                self.db.income_value = i[4]
            elif i[2] == "city_list":
                self.db.city_list_ops = i[3]
                self.db.city_list_value = i[4]
            else:
                logger.error("Config Key Not Found")



