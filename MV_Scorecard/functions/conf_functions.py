import numpy as np
import math
from datetime import datetime, date
from functools import reduce
import logging
import os
import re
from common_functions import *
from fuzzywuzzy import fuzz
import requests

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def acct_inquiry_type_cleaner_cibil(tradelines, inquiry_data):
    for row in tradelines:
        try:
            if row["acct_type_id_cibil"] in ["05"]:
                row['loan_type'], row['acct_type_id'] = "PL", 'A12'
            elif row["acct_type_id_cibil"] in ["02", "03"]:
                row['loan_type'], row['acct_type_id'] = "HL", "A06"
            elif row["acct_type_id_cibil"] in ['10', '16', '31']:
                row['loan_type'], row['acct_type_id'] = "CC", "A15"
            elif row["acct_type_id_cibil"] in ['06']:
                row['loan_type'], row['acct_type_id'] = "CL", "A13"
            elif row["acct_type_id_cibil"] in ['01']:
                row['loan_type'], row['acct_type_id'] = "AL", "A01"
            else:
                row['loan_type'], row['acct_type_id'] = "Other", "A32"
        except Exception as E:
            row['loan_type'], row['acct_type_id'] = "Other", "A32"

    for row in inquiry_data:

        try:
            if row["inquiry_type_cibil"] in ["05"]:
                row['inquiry_type'], row['acct_type_id'] = "PL", 'A12'
            elif row["inquiry_type_cibil"] in ["02", "03"]:
                row['inquiry_type'], row['acct_type_id'] = "HL", "A06"
            elif row["inquiry_type_cibil"] in ['10', '16', '31']:
                row['inquiry_type'], row['acct_type_id'] = "CC", "A15"
            elif row["inquiry_type_cibil"] in ['06']:
                row['inquiry_type'], row['acct_type_id'] = "CL", "A13"
            elif row["inquiry_type_cibil"] in ['01']:
                row['inquiry_type'], row['acct_type_id'] = "AL", "A01"
            else:
                row['inquiry_type'], row['acct_type_id'] = "Other", "A32"
        except Exception as E:
            row['inquiry_type'], row['acct_type_id'] = "Other", "A32"
    return tradelines, inquiry_data


def dpd_cleaner_cibil(tradelines):
    for row in tradelines:
        start_date = datetime.strptime(row['dpd_start_date'], '%Y-%m-%d')
        end_date = datetime.strptime(row['dpd_end_date'], '%Y-%m-%d')
        dpd = str(row['dpd_hist']).upper().replace(' ', '')
        dpd_list = [dpd[x - 3:x] for x in range(3, len(dpd) + 3, 3)]
        dpd_date_dict = {}
        i = start_date
        count = 0
        while i >= end_date and count < len(dpd_list):
            dpd_date_dict[i.strftime('%Y-%m-%d')] = dpd_list[count]
            count += 1
            i = i - dateutil.relativedelta.relativedelta(months=int(1))
        row['dpd_date_dict'] = dpd_date_dict
    return tradelines


def deep_get(dictionary, keys, default=None):
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) and d.get(key) is not None else default,
                  keys.split("."), dictionary)


def on_us_mob_fun(on_us_loan_date):
    # try:
    #     url = os.environ['current_date_api']
    #     r = requests.get(url=url)
    #     print('success', r.json())
    #     try:
    #         current_date = datetime.strptime(r.json()[0].get("applicationdate"), '%d-%m-%Y').date()
    #     except:
    #         current_date = datetime.strptime(r.json()[0].get("applicationdate"), '%Y-%m-%d').date()
    # except:
    current_date = date.today()

    if on_us_loan_date is None or on_us_loan_date in [""]:
        on_us_loan_date = None
    on_us_loan_date = str(on_us_loan_date).replace('/', '-').replace(' ', '').replace(',', '')
    if on_us_loan_date is None or on_us_loan_date in ["", "None"]:
        on_us_loan_date = None
    try:
        on_us_loan_date = datetime.strptime(on_us_loan_date, '%d-%m-%Y')
    except:
        try:
            on_us_loan_date = datetime.strptime(on_us_loan_date, '%Y-%m-%d')
        except Exception as exception:
            on_us_loan_date = None

    try:
        on_us_mob = np.floor((current_date - on_us_loan_date.date()).days / 30)
        return on_us_mob, current_date
    except:
        return None, current_date


def overdue_fun(row):
    try:
        if not math.isnan(row['overdue_amt']) and row['overdue_amt'] not in [None] and int(
                row['overdue_amt']) > \
                5000 and row['live_account'] in [1]:
            count = 1
        else:
            count = 0
    except:
        count = 0

    return count


# def acct_type_cleaner(acct_type_dict, tradelines):
#     for row in tradelines:
#         row['acct_type_id'] = acct_type_dict[row['acct_type_id']][0]
#     return tradelines


def acct_type_cleaner(acct_type_dict, tradelines):
    msg = None
    for row in tradelines:
        try:
            if int(row['acct_type_id']) == 0:
                row['acct_type_id'] = "A32"
            else:
                row['acct_type_id'] = acct_type_dict[int(row['acct_type_id'])][0]
        except Exception as E:
            logger.info(E)
            row['acct_type_id'] = "A32"
    return tradelines


def live_indicator_fun(row):
    if row['current_bal'] <= 0.0 or row['current_bal'] in ["", None]:
        live_indicator = 0
    else:
        live_indicator = 1
    return live_indicator


def days_calculator(birth_date):
    if birth_date is None or birth_date in [""]:
        return None
    birth_date = birth_date.replace('/', '-').replace(' ', '').replace(',', '')
    if birth_date is None or birth_date in [""]:
        return None
    try:
        birth_date = datetime.strptime(birth_date, '%d-%m-%Y')
        today = datetime.today()
        days = (today - birth_date).days
        return int(days)
    except Exception as exception:
        return None


# def last_payment_days_fun(row):
#     try:
#         x = (row['reported_date'] - row['last_payment_date']).days
#     except:
#         x = 0
#     return x

def industry_str_cleaner(string):
    if string is not None:
        rm_slash = string.replace(" / ", " ").replace("/ ", " ").replace(" /", " ").replace("/",
                                                                                            " ")
        pipe_str = rm_slash.replace(" ", "|").replace("(", "").replace(
            ")", "").replace(",", "").replace("-", "").upper()
        return pipe_str
    else:
        return None


def calculated_variables(tadelines):
    live_account_list = []
    pol_only_gold_loan_list = []
    pol_bl_loan_list = []
    pol_only_guarantor_list = []
    no_derog_24mths_list = []
    pol_no_derog_ever_list = []
    nbr_pl_3mths_list = []
    nbr_pl_6mths_list = []
    pol_only_CD_list = []
    pol_90_plus_ever_list = []
    pol_60_plus_12mth_list = []
    pol_60_plus_6mth_list = []
    pol_0_plus_current = []
    nbr_pl_12mths_list = []
    mob_list = []
    max_mob_all_list = []
    pol_90_plus_12mths_list = []
    pol_30_plus_3mths_list = []
    pol_ln_50K_6mth_list = []

    for row in tadelines:
        live_account_list.append(0 if row['live_account'] is None else row['live_account'])
        pol_only_gold_loan_list.append(row['pol_only_gold_loan'])
        pol_bl_loan_list.append(row['pol_bl_loan'])
        pol_only_guarantor_list.append(row['pol_only_guarantor'])
        no_derog_24mths_list.append(row['no_derog_24mths'])
        pol_no_derog_ever_list.append(row['pol_no_derog_ever'])
        nbr_pl_3mths_list.append(row['nbr_pl_3mths'])
        nbr_pl_6mths_list.append(row['nbr_pl_6mths'])
        pol_only_CD_list.append(row['pol_only_CD'])
        pol_90_plus_ever_list.append(row['pol_90_plus_ever'])
        pol_60_plus_12mth_list.append(row['pol_60_plus_12mth'])
        pol_60_plus_6mth_list.append(row['pol_60_plus_6mth'])
        pol_30_plus_3mths_list.append(row['pol_30_plus_3mth'])
        pol_90_plus_12mths_list.append(row['pol_90_plus_12mth'])
        pol_0_plus_current.append(row['pol_0_plus_current'])
        nbr_pl_12mths_list.append(row['nbr_pl_12mths'])
        mob_list.append(row["mob"])
        max_mob_all_list.append(row['max_mob_all'])
        pol_ln_50K_6mth_list.append(row['ln_50K_6mth'])

    return live_account_list, pol_only_gold_loan_list, pol_bl_loan_list, pol_only_guarantor_list, \
        no_derog_24mths_list, pol_no_derog_ever_list, nbr_pl_3mths_list, nbr_pl_6mths_list, \
        pol_only_CD_list, pol_90_plus_ever_list, pol_60_plus_12mth_list, pol_60_plus_6mth_list, \
        pol_0_plus_current, nbr_pl_12mths_list, mob_list, max_mob_all_list, \
        pol_90_plus_12mths_list, pol_30_plus_3mths_list, pol_ln_50K_6mth_list


def name_cleaner(name):
    new_name = ''
    if name is not None:
        new_name = re.sub(r'^(mr|mister|mrs|miss|dr|prof)[\.\s]*', r'', name, count=1,
                          flags=re.IGNORECASE).strip().lower()
    return new_name


def emi_fun_mv(emi_for_acctypes, row):
    try:
        roi_tenure_dict = [value for key, value in emi_for_acctypes.items() if key == row['acct_type_id']][0]
    except Exception as E:
        roi_tenure_dict = {}
    if not roi_tenure_dict:
        roi = 0
        tenure = 0
    else:
        roi = roi_tenure_dict['ROI']
        tenure = roi_tenure_dict['Tenure(yr)']

    loan_amt = row['sanctioned_amount']
    try:
        return int(emi_calculator(float(roi), (float(tenure) * 12), loan_amt))
    except:
        return 0


def acct_inquiry_type_cleaner(acct_type_dict, tradelines, inquiry_data):
    for row in tradelines:
        try:
            if int(row['acct_type_id']) == 0:
                row['acct_type_id'] = "A32"
            else:
                row['acct_type_id'] = acct_type_dict[row['acct_type_id']][0]
        except Exception as E:
            row['acct_type_id'] = "A32"

    for row in inquiry_data:
        try:
            if int(row['inquiry_type']) == 0:
                row['inquiry_type'] = "Other"
            else:
                row['inquiry_type'] = '0'.join(["", row['inquiry_type']]) if len(row['inquiry_type']) == 1 else row[
                    'inquiry_type']
                row['inquiry_type'] = acct_type_dict[row['inquiry_type']][1]
        except Exception as E:
            row['inquiry_type'] = "Other"
    return tradelines, inquiry_data


def nbr_pl_mths_fun_mv(row, mths):
    try:
        if int(row['max_mob_all']) <= mths and row['ownership_tag'] is True and row['acct_type_id'] in ['A12'] and \
                row['live_account'] in [1]:
            return 1
        else:
            return 0
    except Exception as error:
        print(error)
        return 0


def max_mob_all_fun_mv(row):
    try:
        if row['ownership_tag'] is True:
            disbursed_date = datetime.strptime(row['disbursed_dt'], '%Y-%m-%d').date()
            x = row['current_date']
            mob = math.ceil((x - disbursed_date).days / 30)
            return mob
        else:
            return 0
    except Exception as error:
        print(error)
        return 0


def get_posidex_decision(posidex_data, partner):
    mode = posidex_data.get('mode')
    kyc_mode = posidex_data.get('kycMode')
    posidex_response = posidex_data.get('posidexResp')
    matched_customer_details_list = posidex_response.get('MATCHED_CUSTOMER_DETAILS')

    for matched_customer_detail in matched_customer_details_list:
        for matched_criteria_row in matched_customer_detail["MATCH_CRITERIA_LIST"]:
            strength_value = matched_criteria_row.get('STRENGTH', 0)
            matched_criteria_row['STRENGTH'] = strength_value if strength_value not in ("", None) else 0

    pan_match_check = "EXACT MATCH" if any(
        True if 'PAN' in str(matched_criteria_row.get('MATCH_TYPE')).upper() and int(
            matched_criteria_row.get('STRENGTH', 0)) >= 100
        else False
        for matched_customer_detail in matched_customer_details_list
        for matched_criteria_row in matched_customer_detail["MATCH_CRITERIA_LIST"]
    ) else "NO MATCH"

    match_type = pan_match_check
    condition1_check_list = []
    condition2_check_list = []
    condition3_check_list = []
    match_type_list = []
    pol_conditon1_check = None
    pol_conditon2_check = None
    pol_conditon3_check = None
    '''(NAME.MaxStrength >= 95 and DOB.MaxStrength >= 75) 
            or 
            (NAME.MaxStrength >= 95 and ADDRESS.MaxStrength >= 76)
             or 
            (50 <= NAME.MaxStrength <=94 and 50 <= ADDRESS.MaxStrength <=75 and 50 <= DOB.MaxStrength <= 74)'''

    if pan_match_check == "NO MATCH":

        for matched_customer_row in matched_customer_details_list:
            addresscity_strength_list = []
            dob_strength_list = []
            name_strength_list = []
            match_criteria_list = matched_customer_row.get('MATCH_CRITERIA_LIST')
            for matched_criteria_row in match_criteria_list:
                if str(matched_criteria_row.get('MATCH_TYPE')).upper() == 'NAME':
                    name_strength_list.append(float(matched_criteria_row.get('STRENGTH', 0)))
                if str(matched_criteria_row.get('MATCH_TYPE')).upper() == 'DOB':
                    dob_strength_list.append(float(matched_criteria_row.get('STRENGTH', 0)))
                if str(matched_criteria_row.get('MATCH_TYPE')).upper() == 'ADDRESSCITY':
                    addresscity_strength_list.append(float(matched_criteria_row.get('STRENGTH', 0)))

            name_max_stength = max(name_strength_list, default=0)
            dob_max_stength = max(dob_strength_list, default=0)
            addresscity_max_stength = max(addresscity_strength_list, default=0)

            condition1_check = name_max_stength >= 95 and dob_max_stength >= 75
            condition1_check_list.append(condition1_check)
            condition2_check = name_max_stength >= 95 and addresscity_max_stength >= 76
            condition2_check_list.append(condition2_check)
            condition3_check = (50 <= name_max_stength <= 94) and \
                               (50 <= addresscity_max_stength <= 75) and (50 <= dob_max_stength <= 74)
            condition3_check_list.append(condition3_check)

            if condition1_check or condition2_check or condition3_check:
                match_type_list.append("PARTIAL MATCH")

        match_type = "PARTIAL MATCH" if any(True for i in match_type_list if i == "PARTIAL MATCH") else match_type

        pol_conditon1_check = any(condition1_check_list)
        pol_conditon2_check = any(condition2_check_list)
        pol_conditon3_check = any(condition3_check_list)

    posidex_risk_category = 'HIGH' if match_type in ["EXACT MATCH", "PARTIAL MATCH"] else 'LOW'

    kyc_mode_risk_category = 'LOW' if kyc_mode is not None and kyc_mode.upper() in ['DKYC'] else 'HIGH'

    mode_risk_category = 'HIGH' if mode is not None and mode.upper() in ['DIGITAL'] else 'LOW'

    partner_risk_category = 'HIGH' if partner is not None and partner.upper() in ['MV'] else 'LOW'

    partner_plus_mode_risk_category = 'HIGH' if any([partner_risk_category == 'HIGH', mode_risk_category == 'HIGH']) \
        else 'LOW'

    '''decisioning grids'''

    if partner_plus_mode_risk_category == 'HIGH' and \
            kyc_mode_risk_category == 'HIGH' and \
            posidex_risk_category == 'LOW':
        final_risk_category = 'HIGH'
        pol_decision_status = 'Accept'
        rejection_reason = []
    elif partner_plus_mode_risk_category == 'HIGH' and \
            kyc_mode_risk_category == 'LOW' and \
            posidex_risk_category == 'LOW':
        final_risk_category = 'HIGH'
        pol_decision_status = 'Accept'
        rejection_reason = []
    elif partner_plus_mode_risk_category == 'LOW' and \
            kyc_mode_risk_category == 'HIGH' and \
            posidex_risk_category == 'LOW':
        final_risk_category = 'HIGH'
        pol_decision_status = 'Accept'
        rejection_reason = []
    elif partner_plus_mode_risk_category == 'LOW' and \
            kyc_mode_risk_category == 'LOW' and \
            posidex_risk_category == 'LOW':
        final_risk_category = 'LOW'
        pol_decision_status = 'Accept'
        rejection_reason = []
    else:
        final_risk_category = 'HIGH'
        pol_decision_status = 'Decline'
        rejection_reason = [f"posidex_risk_category is {posidex_risk_category},"
                            f"kyc_mode_risk_category is {kyc_mode_risk_category},"
                            f"partner or mode_risk_category is {partner_plus_mode_risk_category}"]

    return {"mode": mode,
            "kyc_mode": kyc_mode,
            "match_type": match_type,
            "posidex_risk_category": posidex_risk_category,
            "kyc_mode_risk_category": kyc_mode_risk_category,
            "mode_risk_category": mode_risk_category,
            "partner_risk_category": partner_risk_category,
            "partner_plus_mode_risk_category": partner_plus_mode_risk_category,
            "final_risk_category": final_risk_category,
            "pol_decision_status": pol_decision_status,
            "rejection_reason": rejection_reason,
            "pan_match_check": pan_match_check,
            "pol_condition1_check": pol_conditon1_check,
            "pol_condition2_check": pol_conditon2_check,
            "pol_condition3_check": pol_conditon3_check
            }, {}


def get_hunter_decision(request, connection):
    warnings = {}
    try:
        # create a cursor
        cursor = connection.cursor()
    except Exception as e:
        logger.error(e)
        warnings["DataBase"] = "database connection break!"
        return None, warnings

    if str(os.environ["env"]).lower() in ["production", "prod"]:
        query = "select response_json from req_resp_scorecards where stage = 'bureau' and loan_app_id = '{}' order by id desc;".format(
            request["applicationId"])
    else:
        query = "select response_json from req_resp_scorecards where application_stage = 'bureau' and loan_app_id = '{}' order by id desc;".format(
            request["applicationId"])

    cursor.execute(query)
    data = cursor.fetchone()
    bureau_response = {}
    if data is not None and 'NA' not in data[0] and len(data) != 0:
        try:
            bureau_response = data[0]['primeResponse']['bureau_response']
        except:
            bureau_response = data[0]['bureau_response']
    else:
        warnings[f'{request["applicationId"]}'] = "Not found in database for bureau stage"
    calculated_variables_dict = bureau_response.get('calculated_variables', {})
    eligibility = bureau_response.get("eligibility", [
        {"pf": 0, "roi": 0, "loanAmount": 0, "emi": 0, "tenure": 0}])
    comp_cat1 = calculated_variables_dict.get('comp_cat1', None)
    final_tradeline_name = calculated_variables_dict.get('final_tradeline_name', 'OTHER')
    bureau_score = calculated_variables_dict.get('bureau_score', None)
    final_bureau_loan_amount = calculated_variables_dict.get('final_loan_amount', 0.0)
    final_roi = calculated_variables_dict.get('final_roi', 0.0)
    final_pf = calculated_variables_dict.get('final_pf', 0)
    final_tenure = calculated_variables_dict.get('final_tenure', 0)
    ntc_tag = deep_get(bureau_response.get('calculated_variables'), 'no_hit', False)
    risk_segment = deep_get(bureau_response.get('calculated_variables'), 'segment')
    bureau_vintage = float(
        deep_get(bureau_response.get('calculated_variables'), 'cust_bureau_vintage', 0))
    applicant = request['report']['applicant']
    partner = request.get('partner', None)

    try:
        loan_amount = float(str(request["report"]["loanDetails"]["loanAmount"]).replace(',', ''))
    except:
        loan_amount = 0

    try:
        user_loan_amount = float(str(applicant['qde']["userLoanAmount"]).replace(',', ''))
    except:
        user_loan_amount = None

    requested_loan_amount = float(loan_amount) if user_loan_amount in [None, 0.0] else float(
        user_loan_amount)

    final_loan_amount = float(requested_loan_amount) if float(final_bureau_loan_amount) in [0.0] else \
        float(min(final_bureau_loan_amount, requested_loan_amount))

    try:
        requested_roi = float(str(request["report"]["loanDetails"].get("receivedROI", '0')).replace('%', ''))
    except:
        requested_roi = 0

    try:
        requested_tenure = float(str(request["report"]["loanDetails"].get("receivedTenure", '0')))
    except:
        requested_tenure = 0

    try:
        requested_pf = float(str(request["report"]["loanDetails"].get("pf", '0')))
    except:
        requested_pf = 0

    # if final_tradeline_name not in ["OTHER"]:
    #     try:
    #         eligibility[0]["loanAmount"] = round(final_loan_amount)
    #     except:
    #         eligibility = eligibility
    #     if partner in ['IL']:
    #         final_roi, final_pf, final_tenure = il_roi_pf_tenure_oct_21(final_tradeline_name, comp_cat1, bureau_score, final_eligibility_amount =final_loan_amount)
    #     else:
    #         final_roi, final_pf, final_tenure = roi_pf_tenure_oct21(final_tradeline_name, comp_cat1,
    #                                                       bureau_score,
    #                                                       final_eligibility_amount=final_loan_amount)
    # else:
    #     final_pf = 2.5
    #     final_roi = 19.99
    #     final_tenure = 36
    #     eligibility[0]["loanAmount"] = round(final_loan_amount)

    final_emi = (eligibility[0]["loanAmount"] and final_roi and final_tenure) and int(
        round(
            abs(float(np.pmt((final_roi / 1200), final_tenure, -(eligibility[0]["loanAmount"])))))) or 0

    try:
        eligibility[0]["pf"] = final_pf
        eligibility[0]["roi"] = final_roi
        eligibility[0]["emi"] = final_emi
        eligibility[0]["tenure"] = final_tenure
    except:
        eligibility = eligibility

    '''banking_trigger'''
    if final_bureau_loan_amount <= 500000:
        banking_trigger = calculated_variables_dict.get('banking_trigger', False)
    elif final_bureau_loan_amount > 500000 and requested_loan_amount <= 500000 and calculated_variables_dict.get(
            'banking_trigger_foir', False):
        banking_trigger = True
    elif min(final_bureau_loan_amount, requested_loan_amount) > 500000:
        banking_trigger = True
    else:
        banking_trigger = False

    pol_hunter_decision = "Accept"
    reasons = []
    reject_review_tag = False
    try:
        hunterScore = str(
            applicant["hunterData"]["hunterData"]["response"]["response"]["matchResponse"][
                "matchResult"][
                "resultBlock"]["matchSummary"][
                "totalMatchScore"]).replace(",", "")
    except:
        try:
            hunterScore = str(
                applicant["hunterData"]["hunterData"]["matchResponse"]["matchResult"][
                    "resultBlock"][
                    "matchSummary"][
                    "totalMatchScore"]).replace(",", "")
        except:
            hunterScore = None

    if hunterScore is not None:
        if str(hunterScore).strip() in ["", "None"]:
            hunter_score = None
        else:
            hunter_score = float(hunterScore)

        if (hunter_score is not None and (
                (final_loan_amount <= 200000 and int(hunter_score) <= 750) or (
                final_loan_amount in range(200001, 500001) and int(hunter_score) <= 700) or (
                        final_loan_amount >= 500001 and int(hunter_score) <= 700))):
            pol_hunter_decision = "Decline"
        elif ((hunter_score is not None and (
                final_loan_amount in range(70000, 500001) and int(hunter_score) > 700) and (
                       ntc_tag is True or risk_segment in ["A3",
                                                           "A4"] or bureau_vintage < 72)) or (
                      hunter_score is not None and final_loan_amount > 500000)):
            pol_hunter_decision = "Refer"
        else:
            pol_hunter_decision = "Accept"

        if pol_hunter_decision in ["Decline"]:
            if final_loan_amount <= 200000 and int(hunter_score) <= 750:
                reasons.append(
                    f"final loan amount: {final_loan_amount} is <= 2 Lac and hunter score {hunter_score} is <= 750")
            elif final_loan_amount in range(200001, 500001) and int(hunter_score) <= 700:
                reasons.append(
                    f"final loan amount: {final_loan_amount} is in range 2Lac to 5Lac and hunter score {hunter_score} is <= 700")
            elif final_loan_amount >= 500001 and int(hunter_score) <= 700:
                reasons.append(
                    f"final loan amount: {final_loan_amount} is >= 5Lac and hunter score {hunter_score} is <= 700")
        elif pol_hunter_decision in ["Refer"]:
            reject_review_tag = True
            if final_loan_amount in range(70000, 500001) and int(
                    hunter_score) > 700 and ntc_tag:
                reasons.append(
                    f"final loan amount: {final_loan_amount} is in range 70k to 5Lac and hunter score {hunter_score} is > 700 and NTC is True")
            elif final_loan_amount in range(70000, 500001) and int(
                    hunter_score) > 700 and risk_segment in ["A3", "A4"]:
                reasons.append(
                    f"final loan amount: {final_loan_amount} is in range 70k to 5Lac and hunter score {hunter_score} is > 700 and segment is {risk_segment}")
            elif final_loan_amount in range(70000, 500001) and int(
                    hunter_score) > 700 and bureau_vintage < 72:
                reasons.append(
                    f"final loan amount: {final_loan_amount} is in range 70k to 5Lac and hunter score {hunter_score} is > 700 and bureau_vintage is {bureau_vintage}")
            elif hunter_score is not None and final_loan_amount > 500000:
                reasons.append(
                    f"hunter_score is {hunter_score} and final loan amount : {final_loan_amount} is > 500000")

    calculated_variables_dict["pol_bureau_final_decision_status"] = \
        calculated_variables_dict.get("pol_decision_status", "Decline")
    calculated_variables_dict["pol_decision_status"] = pol_hunter_decision
    calculated_variables_dict["pol_hunter_decision"] = pol_hunter_decision
    calculated_variables_dict["hunter_score"] = hunterScore
    calculated_variables_dict['banking_trigger'] = banking_trigger

    return {"calculated_variables": calculated_variables_dict,
            "pol_decision_status": pol_hunter_decision,
            "rejection_reason": reasons,
            "reject_review_tag": reject_review_tag,
            "banking_trigger": calculated_variables_dict.get('banking_trigger'),
            "ev_perfios_match": calculated_variables_dict.get('ev_perfios_match', 'Decline'),
            "ev_bureau_match": calculated_variables_dict.get('ev_bureau_match', False),
            "eligibility": eligibility}, warnings


# ======================cibil==========================================
def dpd_cleaner_cibil(tradelines, current_date):
    for row in tradelines:
        start_date = datetime.strptime(row['dpd_start_date'], '%Y-%m-%d')
        end_date = datetime.strptime(row['dpd_end_date'], '%Y-%m-%d')
        if int(start_date.month) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
            updated_start_date = date(int(start_date.year), int(start_date.month), 28)
        else:
            updated_start_date = date(int(start_date.year), int(start_date.month), int(current_date.day))

        if int(end_date.month) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
            updated_end_date = date(int(end_date.year), int(end_date.month), 28)
        else:
            updated_end_date = date(int(end_date.year), int(end_date.month), int(current_date.day))

        dpd = str(row['dpd_hist']).upper().replace(' ', '')
        dpd_list = [dpd[x - 3:x] for x in range(3, len(dpd) + 3, 3)]
        dpd_date_dict = {}
        i = updated_start_date
        count = 0
        while i >= updated_end_date and count < len(dpd_list):
            dpd_date_dict[i.strftime('%Y-%m-%d')] = dpd_list[count]
            count += 1
            i = i - dateutil.relativedelta.relativedelta(months=int(1))
        row['dpd_date_dict'] = dpd_date_dict
    return tradelines


def no_derog_24mths_fun_new(row, type_of_xml):
    try:
        if type_of_xml in ["MB"]:
            if row['ownership_tag'] is True and (
                    (row['suit_filed_wilful_default'] and int(row['suit_filed_wilful_default']) in range(2, 5)) or int(
                row['write_off_settled']) in range(5, 16)) and (row['mob'] <= 24):
                return 1
            else:
                return 0
        elif type_of_xml == "EXP":
            if row['ownership_tag'] is True and (np.any(int(row['suit_filed_wilful_default']) in [89, 93]) or np.any(
                    int(row['write_off_settled']) in [30, 31, 97, 32, 33, 34, 35, 36, 37, 38,
                                                      39])) and (row['mob'] <= 24):
                return 1
            else:
                return 0
        if type_of_xml in ["CIBIL", "CIBIL_JSON"]:
            if row['ownership_tag'] is True and (
                    (row['suit_filed_wilful_default'] and int(row['suit_filed_wilful_default']) in range(1, 4)) or int(
                row['write_off_settled']) in [2, 3, 4, 6, 8, 9, 13, 14, 15, 16]) and (row['mob'] <= 24):
                return 1
            else:
                return 0
    except Exception as error:
        return 0


def pol_no_derog_ever_fun_new(row, type_of_xml):
    try:
        if row['suit_filed_wilful_default'] in [None, np.nan]:
            suit_filed_wilful_default = 0
        else:
            suit_filed_wilful_default = int(row['suit_filed_wilful_default'])

        if row['write_off_settled'] in [None, np.nan]:
            write_off_settled = 0
        else:
            write_off_settled = int(row['write_off_settled'])

        if row['write_off_principal'] in [None, np.nan]:
            write_off_principal = 0
        else:
            write_off_principal = int(row['write_off_principal'])

        if row['settlement_amt'] in [None, np.nan]:
            settlement_amt = 0
        else:
            settlement_amt = int(row['settlement_amt'])

        if row['write_off_total'] in [None, np.nan]:
            write_off_total = 0
        else:
            write_off_total = int(row['write_off_total'])

        if row['write_off_principal'] in [None, np.nan]:
            write_off_principal = 0
        else:
            write_off_principal = int(row['write_off_principal'])

        if type_of_xml in ["MB"]:
            if row['ownership_tag'] is True and (int(suit_filed_wilful_default) in range(2, 5) or
                                                 int(write_off_settled) in range(5, 16) and
                                                 (settlement_amt > 5000 or write_off_total > 5000 or
                                                  write_off_principal > 5000)):
                return 1
            else:
                return 0
        elif type_of_xml == "EXP":
            if row['ownership_tag'] is True and (np.any(suit_filed_wilful_default in [89, 93]) or np.any(
                    write_off_settled in [30, 31, 97, 32, 33, 34, 35, 36, 37, 38, 39] and
                    (settlement_amt > 5000 or write_off_total > 5000 or
                     write_off_principal > 5000))):
                return 1
            else:
                return 0
        if type_of_xml in ["CIBIL", "CIBIL_JSON"]:
            if row['ownership_tag'] is True and (int(suit_filed_wilful_default) in range(1, 4) or
                                                 int(write_off_settled) in [2, 3, 4, 6, 8, 9, 13, 14, 15, 16] and
                                                 (settlement_amt > 5000 or write_off_total > 5000 or
                                                  write_off_principal > 5000)):
                return 1
            else:
                return 0
    except Exception as error:
        return 0


# def pol_90_plus_ever_fun_new(row, type_of_xml):
#     count = 0
#     if type_of_xml == "MB":
#         for i in row['dpd_dates']:
#             i = i.split('-')
#             asset = i[2].split('/')[1]
#             i[2] = i[2].split('/')[0]
#             if i[2] in ["xxx", "XXX"]:
#                 i[2] = 0
#             if int(score_dpd1(row, int(i[2]), asset)) >= 90 and row['ownership_tag'] is True:
#                 count = 1
#             else:
#                 count = 0
#             if count == 1:
#                 return 1
#         if count is 0:
#             return 0
#     elif type_of_xml == "EXP":
#         for i in row['dpd_list']:
#             if i is None:
#                 i = 0
#             if int(score_dpd_exp(row, int(i))) >= 90 and row['ownership_tag'] is True:
#                 count = 1
#             else:
#                 count = 0
#             if count == 1:
#                 return 1
#         if count is 0:
#             return 0
#     elif type_of_xml in ["CIBIL_JSON", "CIBIL"]:
#         count = 0
#         # current_date = datetime.strptime(current_date, "%Y-%m-%d").date()    # Only for Experian
#         try:
#             for k, v in row['dpd_date_dict'].items():
#                 if v.isdigit():
#                     p_dpd = int(v)
#                     asset = None
#                 else:
#                     p_dpd = 0
#                     asset = v
#                 if int(score_dpd_cibil(row, p_dpd, asset)) >= 90 and row['ownership_tag'] is True:
#                     count = 1
#                 else:
#                     count = 0
#                 if count == 1:
#                     return 1
#             if count is 0:
#                 return 0
#         except Exception as error:
#             logger.error(error)
#             return 0

def pol_90_plus_ever_fun_new(row, type_of_xml):
    count = 0
    if type_of_xml == "MB":
        for i in row['dpd_dates']:
            i = i.split('-')
            asset = i[2].split('/')[1]
            i[2] = i[2].split('/')[0]
            if i[2] in ["xxx", "XXX"]:
                i[2] = 0
            if int(score_dpd1(row, int(i[2]), asset)) >= 90 and row['ownership_tag'] is True:
                count = 1
            else:
                count = 0
            if count == 1:
                return 1
        if count == 0:
            return 0
    elif type_of_xml == "EXP":
        for i, m, y, asset in zip(row['Days_Past_Due'], row['reported_month'], row['reported_year'],
                                  row['Asset_classification']):
            if i is None:
                i = 0
            if int(score_dpd_exp1(row, int(i), asset)) >= 90 and row['ownership_tag'] is True:
                count = 1
            else:
                count = 0
            if count == 1:
                return 1
        if count == 0:
            return 0
    elif type_of_xml in ["CIBIL_JSON", "CIBIL"]:
        count = 0
        # current_date = datetime.strptime(current_date, "%Y-%m-%d").date()    # Only for Experian
        try:
            for k, v in row['dpd_date_dict'].items():
                if v.isdigit():
                    p_dpd = int(v)
                    asset = None
                else:
                    p_dpd = 0
                    asset = v
                if int(score_dpd_cibil(row, p_dpd, asset)) >= 90 and row['ownership_tag'] is True:
                    count = 1
                else:
                    count = 0
                if count == 1:
                    return 1
            if count == 0:
                return 0
        except Exception as error:
            logger.error(error)
            return 0


def pol_dpd_plus_mths_fun_new(row, current_date, dpd, months, type_of_xml):
    count = 0
    # current_date = datetime.strptime(current_date, "%Y-%m-%d").date()    # Only for Experian
    today = current_date
    req_date = today - dateutil.relativedelta.relativedelta(months=int(months))
    try:
        if type_of_xml == "MB":
            for i in row['dpd_dates']:
                i = i.split('-')
                i[0] = datetime.strptime(i[0], '%b').month
                if int(i[0]) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
                    d = date(int(i[1]), int(i[0]), 28)
                else:
                    d = date(int(i[1]), int(i[0]), int(current_date.day))
                if req_date <= d <= today:
                    asset = i[2].split('/')[1]
                    i[2] = i[2].split('/')[0]
                    if i[2] in ["xxx", "XXX"]:
                        i[2] = 0
                    if int(score_dpd1(row, int(i[2]), asset)) >= int(dpd) and row['ownership_tag'] is True:
                        count = 1
                    else:
                        count = 0
                if count == 1:
                    return 1
            if count == 0:
                return 0
        elif type_of_xml == "EXP":
            for i, m, y, asset in zip(row['Days_Past_Due'], row['reported_month'], row['reported_year'],
                                      row['Asset_classification']):
                if int(m) in [2, 4, 6, 9, 11] and int(row['current_date'].day) > 28:
                    d = date(int(y), int(m), 28)
                else:
                    d = date(int(y), int(m), int(row['current_date'].day))
                if req_date <= d <= today:
                    if i is None:
                        i = 0
                    if int(score_dpd_exp1(row, int(i), asset)) >= int(dpd) and row['ownership_tag'] is True:
                        count = 1
                    else:
                        count = 0
                if count == 1:
                    return 1
            if count == 0:
                return 0
        elif type_of_xml == 'CIBIL_JSON':
            '''applied in cibil cleaner func 
            if int(m) in [2, 4, 6, 9, 11] and int(row['current_date'].day) > 28:'''
            try:
                for k, v in row['dpd_date_dict'].items():
                    d = datetime.strptime(k, '%Y-%m-%d').date()
                    if req_date <= d <= today:
                        try:
                            p_dpd = int(v)
                            asset = None
                        except:
                            p_dpd = 0
                            asset = v
                        if int(score_dpd_cibil(row, p_dpd, asset)) >= int(dpd) and row['ownership_tag'] is True:
                            count = 1
                        else:
                            count = 0
                    if count == 1:
                        return 1
                if count == 0:
                    return 0
            except Exception as error:
                logger.error(error)
                return 0
    except Exception as error:
        return 0


def score_dpd_cibil(row, p_dpd, asset):
    if p_dpd not in [0] and row['suit_filed_wilful_default'] not in [None, np.nan, ''] and str(
            row['suit_filed_wilful_default']) not in ["nan",
                                                      None, ''] and int(
        row['suit_filed_wilful_default']) in range(1, 4) and row['write_off_principal'] not in ['nan', None,
                                                                                                np.nan] and float(
        row['write_off_principal']) >= 5000.0:
        dpd_new = 180
    elif p_dpd not in [0] and row['write_off_settled'] not in [None, np.nan, ''] and str(
            row['write_off_settled']) not in ["nan", ''] and int(
        row['write_off_settled']) in [2, 3, 4, 6, 8, 9, 13, 14, 15, 16] and row['write_off_principal'] not in [
        'nan', None, '',
        np.nan] and float(
        row['write_off_principal']) >= 5000.0:
        dpd_new = 180
    elif bool({asset} & {'LOS', 'DBT'}) or p_dpd >= 180:
        dpd_new = 180
    elif (bool({asset} & {'SUB'}) and row['write_off_principal'] > 3000) or p_dpd >= 90:
        dpd_new = 90
    elif bool({asset} & {'SMA'}) or p_dpd >= 60:
        dpd_new = 60
    elif p_dpd >= 30:
        dpd_new = 30
    elif p_dpd > 0:
        dpd_new = 29
    elif row['overdue_amt'] is not None and int(row['overdue_amt']) > 0:
        dpd_new = 29
    elif p_dpd <= 0:
        dpd_new = 0
    else:
        dpd_new = p_dpd

    # if bool({row['acct_type_id']} & {'A15', 'A16', 'A17', 'A61'}) and int(row['overdue_amt']) <= 5000:
    #     dpd_new = 0
    # else:
    #     pass

    return dpd_new


def pol_0_plus_current_fun_new(row, current_date, dpd, months, type_of_xml):
    count = 0
    # current_date = datetime.strptime(current_date, "%Y-%m-%d").date()    # Only for Experian
    # today = date(current_date.year, current_date.month, current_date.day)
    today = current_date
    req_date = today - dateutil.relativedelta.relativedelta(months=int(months))

    if type_of_xml == "MB":
        for i in row['dpd_dates']:
            i = i.split('-')
            i[0] = datetime.strptime(i[0], '%b').month
            if int(i[0]) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
                d = date(int(i[1]), int(i[0]), 28)
            else:
                d = date(int(i[1]), int(i[0]), int(current_date.day))
            if req_date <= d <= today:
                asset = i[2].split('/')[1]
                i[2] = i[2].split('/')[0]
                if i[2] in ["xxx", "XXX"]:
                    i[2] = 0
                if int(score_dpd1(row, int(i[2]), asset)) > int(dpd) and row['ownership_tag'] is True:
                    count = 1
                else:
                    count = 0
            if count == 1:
                return 1
        if count == 0:
            return 0
    elif type_of_xml == "EXP":
        for i, m, y in zip(row['Days_Past_Due'], row['reported_month'], row['reported_year']):
            if int(m) in [2, 4, 6, 9, 11] and int(current_date.day) > 28:
                d = date(int(y), int(m), 28)
            else:
                d = date(int(y), int(m), int(current_date.day))
            if req_date <= d <= today:
                if i is None:
                    i = 0
                if int(score_dpd_exp(row, int(i))) > int(dpd) and row['ownership_tag'] is True:
                    count = 1
                else:
                    count = 0
            if count == 1:
                return 1
        if count == 0:
            return 0
    elif type_of_xml == 'CIBIL_JSON':
        try:
            for k, v in row['dpd_date_dict'].items():
                d = datetime.strptime(k, '%Y-%m-%d').date()
                if req_date <= d <= today:
                    try:
                        p_dpd = int(v)
                        asset = None
                    except:
                        p_dpd = 0
                        asset = v
                    if int(score_dpd_cibil(row, p_dpd, asset)) > int(dpd) and row['ownership_tag'] is True:
                        count = 1
                    else:
                        count = 0
                if count == 1:
                    return 1
            if count == 0:
                return 0
        except Exception as error:
            logger.error(error)
            return 0


def define_segment_values(risk_segment, scheme_priority_name, verified_income):
    risk_band_values = {
        "LOW": 1,
        "MEDIUM": 2,
        "HIGH": 3,
        "VERY-HIGH": 4,
        "VERY HIGH": 4,
    }

    scheme_priority_values = {
        "salaried": 5,
        "self-employed": 6,
        "repeat/top-ups": 7,
        "low & grow": 8,
    }

    if verified_income <= 30000:
        verified_income_band = 9
    elif 30000 < verified_income <= 50000:
        verified_income_band = 10
    elif 50000 < verified_income <= 100000:
        verified_income_band = 11
    else:
        verified_income_band = 12

    try:
        risk_band = risk_band_values[risk_segment.upper()]
    except Exception:
        risk_band = "unknown risk"

    try:
        scheme_priority_band = scheme_priority_values[scheme_priority_name.lower()]
    except Exception:
        scheme_priority_band = "unknown scheme priority"

    return risk_band, scheme_priority_band, verified_income_band


def calculate_tenor_cap(R2, S2):
    if R2 == 1 and (S2 == 5 or S2 == 7):
        return 60
    elif R2 == 1 and S2 == 6:
        return 36
    elif R2 == 2 and (S2 == 5 or S2 == 6 or S2 == 7):
        return 24
    elif (R2 in [3, 4]) and S2 == 8:
        return 6
    else:
        return 12


def calculate_loan_amount_cap(R2, S2):
    if R2 == 1 and (S2 == 5 or S2 == 7):
        return 500000
    elif (R2 == 1 and S2 == 6) or (R2 == 2 and (S2 == 5 or S2 == 6 or S2 == 7)):
        return 300000
    elif R2 == 3 and (S2 == 5 or S2 == 6 or S2 == 7):
        return 100000
    elif R2 == 3 and S2 == 8:
        return 15000
    elif R2 == 4 and S2 == 8:
        return 10000
    else:
        return 30000


def calculate_emi_multiplier_cap(R2, S2):
    if R2 == 1 and (S2 in [5, 6, 7, 8]):
        return 1
    elif R2 == 2 and (S2 in [5, 6, 7, 8]):
        return 0.9
    else:
        return 0.75


def calculate_max_emi(income, obligations, emi_multiplier):
    emi_1 = (0.8 * income - obligations) * emi_multiplier
    emi_2 = 0.7 * income - obligations
    return min(emi_1, emi_2)


def calculate_loan_multiplier_cap(R2, verified_income_band):
    if R2 == 1 and verified_income_band in [9, 10, 11, 12]:
        return 7.5
    elif R2 == 2 and verified_income_band == 9:
        return 4
    elif R2 == 2 and verified_income_band in [10, 11]:
        return 4.5
    elif R2 == 2 and verified_income_band == 12:
        return 6
    elif R2 == 3 and verified_income_band == 9:
        return 3.5
    elif R2 == 3 and verified_income_band in [10, 11]:
        return 3.75
    elif R2 == 3 and verified_income_band == 12:
        return 5
    elif R2 == 4 and verified_income_band == 9:
        return 2.7
    elif R2 == 4 and verified_income_band == 10:
        return 2.75
    elif R2 == 4 and verified_income_band == 11:
        return 3
    elif R2 == 4 and verified_income_band == 12:
        return 3.5
    else:
        return 0

def calculate_scheme_priority(scheme_name, loan_flag, employment_type):
    if loan_flag == "TOPUP" or loan_flag == "REPEAT":
        scheme_priority = "Repeat/Top-ups"
    elif loan_flag == "NEW" and scheme_name == "GROWTH":
        scheme_priority = "Low & Grow"
    elif employment_type == "SALARIED":
        scheme_priority = "SALARIED"
    elif employment_type in ["SELF_EMPLOYMENT", "SELF_EMPLOYED", "SELFEMPLOYED", "SELF EMPLOYED"]:
        scheme_priority = "SELF-EMPLOYED"
    else:
        scheme_priority = "Unknown"

    return scheme_priority


def get_tenure_factor(tenure):
    if tenure is None:
        return 1
    if tenure <= 12:
        return 1
    elif 13 <= tenure <=18:
        return 1.5
    elif 19 <= tenure <= 24:
        return 2
    elif 25 <= tenure <= 36:
        return 2.5
    elif 37 <= tenure <= 48:
        return 3
    elif 49 <= tenure <=60:
        return 3.5
