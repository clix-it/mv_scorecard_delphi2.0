import os
import json
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def warning_resp_json(event, env, warnings):
    return {"status": False,
            "loanApplicationId": event["applicationId"],
            "customerId": event["customerId"],
            "product": event["product"],
            "partner": event["partner"],
            "stage": event["report"]["stage"]["applicationStage"].lower(),
            "scorecard": {
                "scorecard_name": f"{env}:MV_Scorecard",
                "scorecard_version": "1.0"
            },
            "warnings": warnings}


def response_json(request, response, tradeline_list=None, warnings=None, application_stage=None):
    product = request["product"]
    partner = request["partner"]
    app_id = request["applicationId"]
    customer_id = request["customerId"]
    policy = "PRIME"

    try:
        match_type = request['report']['applicant']["match_type"]
    except:
        match_type = None

    try:
        pan = request["report"]["applicant"]["qde"]["pan"]
    except:
        pan = None

    if tradeline_list is None:
        tradeline_list = []

    if warnings is None:
        warnings = {}

    if len(warnings) == 0:
        warnings['No warnings'] = 'Correct data'

    if application_stage in ['professional_info']:
        final_loan_amount = None
        policy = response.get('policy_type')
        eligibility = [
            {
                "pf": None,
                "roi": None,
                "loanAmount": None,
                "emi": None,
                "tenure": None
            }
        ]
    elif application_stage in ['hunter', "Hunter", "HUNTER"]:
        try:
            final_loan_amount = response.get("eligibility")[0]["loanAmount"]
        except:
            final_loan_amount = 0
        eligibility = response.get("eligibility", [{
            "pf": 0,
            "roi": 0,
            "loanAmount": 0,
            "emi": 0,
            "tenure": 0
        }])
    else:
        if response["pol_decision_status"] in ['Decline', 'Reject'] and response.get("reject_review_tag") is False:
            final_loan_amount = 0
            eligibility = [
                {
                    "pf": 0,
                    "roi": 0,
                    "loanAmount": 0,
                    "emi": 0,
                    "tenure": 0
                }
            ]
        else:
            final_loan_amount = response.get("final_loan_amount", 0)
            eligibility = [
                {
                    "pf": response.get('final_pf', 0),
                    "roi": response.get('final_roi', 0),
                    "loanAmount": response.get('final_loan_amount', 0),
                    "emi": response.get("final_emi", 0),
                    "tenure": response.get('final_tenure', 0)
                }
            ]
    if application_stage in ['hunter', "Hunter", "HUNTER"]:
        resp_dict = {
            "status": "true",
            "loanApplicationId": app_id,
            "customerId": customer_id,
            "product": product,
            "partner": partner,
            "scorecard": {
                "scorecard_name": f"{os.environ['env']}:{product}_{partner}_Scorecard",
                "scorecard_version": "1.0"
            },
            "bureau_response": {
                "calculated_variables":
                    response.get("calculated_variables"),
                "decision": {
                    "status": response.get("pol_decision_status"),
                    "reasons": response.get("rejection_reason"),
                    # "reject_review": response.get("reject_review_tag"),
                    # "banking_trigger": response.get("banking_trigger"),
                    # "ev_perfios_match": response.get("ev_perfios_match"),
                    # "ev_bureau_match": response.get("ev_comp_bureau_match")
                },
                "tradelines": tradeline_list,
                "eligibility": eligibility,
                "extra_details": {
                    "document_list": [],
                    "applicationStage": application_stage,
                    "match_type": match_type
                }
            },
            "warnings": warnings

        }
    else:
        resp_dict = {
            "status": "true",
            "loanApplicationId": app_id,
            "customerId": customer_id,
            "product": product,
            "partner": partner,
            "scorecard": {
                "scorecard_name": f"{os.environ['env']}:MV_Scorecard",
                "scorecard_version": "1.0"
            },
            "bureau_response": {
                "calculated_variables":
                    response,
                "decision": {
                    "status": response.get("pol_decision_status"),
                    "reasons": response.get("rejection_reason"),
                    # "reject_review": response.get("reject_review_tag"),
                    # "banking_trigger": response.get("banking_trigger"),
                    # "ev_perfios_match": response.get("ev_perfios_match"),
                    # "ev_bureau_match": response.get("ev_comp_bureau_match")
                },
                "tradelines": tradeline_list,
                "eligibility": eligibility,
                "extra_details": {
                    "document_list": [],
                    "applicationStage": application_stage,
                    "match_type": match_type
                }
            },
            "warnings": warnings

        }

    query_dict = {"product": product, "partner": partner, "scorecard_version": "$LATEST", "loan_app_id": app_id,
                  "customer_id": customer_id, "calculated_variables": json.dumps(response),
                  "mobile_no": response.get("mobile_number"), "pan_no": pan,
                  "status": response["pol_decision_status"],
                  "reject_reasons": response["rejection_reason"],
                  "eligibility": json.dumps(resp_dict["bureau_response"]["eligibility"][0]),
                  "loanAmount": final_loan_amount,
                  "extra_details": json.dumps(resp_dict["bureau_response"]["extra_details"]),
                  "warnings": json.dumps(warnings),
                  "stage": application_stage,
                  'policy': policy,
                  }

    lambda_client = boto3.client('lambda')
    log_response = lambda_client.invoke(FunctionName="scorecard_async_logging", InvocationType='Event',
                                        Payload=json.dumps(query_dict))
    log_resp = log_response['Payload'].read().decode()
    logger.info("---------Scoreacrd logs Response---------:")
    logger.info(log_resp)

    return resp_dict
